function SIMUL = SimulationProperties(Building, Location, inputOfInterest, OPTIONS)

nInputs = length(inputOfInterest);
SIMUL.setup = cell(1,nInputs);
SIMUL.version = OPTIONS.version;
SIMUL.echoFlag = OPTIONS.echoFlag;

switch Location
    case 'Miami'
        IDFname2 = '_1A_USA_FL_MIAMI';
        SIMUL.weatherfile = 'USA_FL_Miami.Intl.AP.722020_TMY3';
        ClimateZone = '1A';
    case 'Houston'
        IDFname2 = '_2A_USA_TX_HOUSTON';
        SIMUL.weatherfile = 'USA_TX_Houston-Bush.Intercontinental.AP.722430_TMY3';
        ClimateZone = '2A';
    case 'Phoenix'
        IDFname2 = '_2B_USA_AZ_PHOENIX';
        SIMUL.weatherfile = 'USA_AZ_Phoenix.722780_TMY2';
        ClimateZone = '2B';
    case 'Altanta'
        IDFname2 = '_3A_USA_GA_ATLANTA';
        SIMUL.weatherfile = 'USA_GA_Atlanta.722190_TMY2';
        ClimateZone = '3A';
    case 'Los Angeles'
        IDFname2 = '_3B_USA_CA_LOS_ANGELES';
        SIMUL.weatherfile = 'USA_CA_Los.Angeles.Intl.AP.722950_TMY3';
        ClimateZone = '3B';
    case 'Las Vegas'
        IDFname2 = '_3B_USA_NV_LAS_VEGAS';
        SIMUL.weatherfile = 'USA_NV_Las.Vegas.723860_TMY2';
        ClimateZone = '3B';
    case 'San Francisco'
        IDFname2 = '_3C_USA_CA_SAN_FRANCISCO';
        SIMUL.weatherfile = 'USA_CA_San.Francisco.Intl.AP.724940_TMY3';
        ClimateZone = '3C';
    case 'Baltimore'
        IDFname2 = '_4A_USA_MD_BALTIMORE';
        SIMUL.weatherfile = 'USA_MD_Baltimore.724060_TMY2';
        ClimateZone = '4A';
    case 'Albuquerque'
        IDFname2 = '_4B_USA_NM_ALBUQUERQUE';
        SIMUL.weatherfile = 'USA_NM_Albuquerque.723650_TMY2';
        ClimateZone = '4B';
    case 'Seattle'
        IDFname2 = '_4C_USA_WA_SEATTLE';
        SIMUL.weatherfile = 'USA_WA_Seattle-Tacoma.Intl.AP.727930_TMY3';
        ClimateZone = '4C';
    case 'Chicago'
        IDFname2 = '_5A_USA_IL_CHICAGO-OHARE';
        SIMUL.weatherfile = 'USA_IL_Chicago-OHare.Intl.AP.725300_TMY3';
        ClimateZone = '5A';
    case 'Boulder'
        IDFname2 = '_5B_USA_CO_BOULDER';
        SIMUL.weatherfile = 'USA_CO_Boulder.724699_TMY2';
        ClimateZone = '5B';
    case 'Minneapolis'
        IDFname2 = '_6A_USA_MN_MINNEAPOLIS';
        SIMUL.weatherfile = 'USA_MN_Minneapolis-St.Paul.Intl.AP.726580_TMY3';
        ClimateZone = '6A';
    case 'Helena'
        IDFname2 = '_6B_USA_MT_HELENA';
        SIMUL.weatherfile = 'USA_MT_Helena.727720_TMY2';
        ClimateZone = '6B';
    case 'Duluth'
        IDFname2 = '_7A_USA_MN_DULUTH';
        SIMUL.weatherfile = 'USA_MN_Duluth.Intl.AP.727450_TMY3';
        ClimateZone = '7A';
    case 'Fairbanks'
        IDFname2 = '_8A_USA_AK_FAIRBANKS';
        SIMUL.weatherfile = 'USA_AK_Fairbanks.Intl.AP.702610_TMY3';
        ClimateZone = '8A';
end

switch Building
    case 'Large Office'
        %% General informations
        IDFname1 = 'RefBldgLargeOfficeNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_largeoffice_new2004_v1-4_7-2\';
        SIMUL.nrZones = 19;
        SIMUL.TotalFloorArea = 46320;
        SIMUL.TotalWindowArea = 4636;
        SIMUL.MaxMultiplier = 1.5; %4646/6954
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype= 'NONE';
                    else
                        SIMUL.setup{i}.surfacetype= 'Mass NonRes Wall Insulation';
                    end
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,GROUNDFLOOR_PLENUM_WALL_EAST,Wall,';
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,BUILDING_ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_TOP_ZN_4_WALL_WEST_WINDOW,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Medium Office'
        %% General informations
        IDFname1 = 'RefBldgMediumOfficeNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_mediumoffice_new2004_v1-4_7-2\';
        SIMUL.nrZones = 18;
        SIMUL.TotalFloorArea = 4982;
        SIMUL.TotalWindowArea = 652.8;
        SIMUL.MaxMultiplier = 2; %652.8/1325 %CONTROLLA!
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'Steel Frame NonRes Wall Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_BOT_PLENUM_WALL_EAST';
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,BUILDING_ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_BOT_ZN_1_WALL_SOUTH_WINDOW,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Small Office'
        %% General informations
        IDFname1 = 'RefBldgSmallOfficeNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_smalloffice_new2004_v1-4_7-2\';
        SIMUL.nrZones = 6;
        SIMUL.TotalFloorArea = 511;
        SIMUL.TotalWindowArea = 55.8;
        SIMUL.MaxMultiplier = 4.5;
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype= 'NONE';
                    else
                        SIMUL.setup{i}.surfacetype= 'Mass NonRes Wall Insulation';
                    end
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_ZN_3_WALL_NORTH';
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'AtticFloor NonRes Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,ATTIC_FLOOR_PERIMETER_EAST,Floor,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_ZN_1_WALL_SOUTH_WINDOW_1,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
                    %case 'Window Height'
                    %    % CORRESPONDING TO: Window Size
                    %    SIMUL.setup{end+1} = [];
                    %    SIMUL.setup{end}.section = 'FenestrationSurface:Detailed';
                    %    SIMUL.setup{end}.surfacetype= 'Window,                  !- Surface Type';
                    %    SIMUL.setup{end}.parameter = {'Vertex 1 Z-coordinate',...
                    %                                  'Vertex 2 Z-coordinate',...
                    %                                  'Vertex 3 Z-coordinate',...
                    %                                  'Vertex 4 Z-coordinate';
                    %                                  2 , 1 , 1 , 2}; % first row is the name, second row is the value to be used
                    %    SIMUL.setup{end}.nparameter = length(SIMUL.setup{end}.parameter);
                    %    SIMUL.setup{end}.type = 'Surface Configuration Parameter';
                    %    SIMUL.setup{end}.range = [0.2 , 1.1; 2.4 , 2.8];
                    %    SIMUL.setup{end}.distribution = 'Uniform';
            end
        end
        
    case 'Warehouse'
        %% General Informations
        IDFname1 = 'RefBldgWarehouseNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_warehouse_new2004_v1-4_7-2\';
        SIMUL.nrZones = 3;
        SIMUL.TotalFloorArea = 4835;
        SIMUL.TotalWindowArea = 17.7;
        SIMUL.MaxMultiplier = 4.5; %% TODO
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Metal Building Semi-Cond Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_ZN_3_WALL_NORTH'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'Metal Semi-Cond Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,ATTIC_FLOOR_PERIMETER_EAST,Floor,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,PERIMETER_ZN_1_WALL_SOUTH_WINDOW_1,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
    case 'Stand-alone Retail'
        IDFname1 = 'RefBldgStand-aloneRetailNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_stand-aloneretail_new2004_v1-4_7-2\';
        SIMUL.nrZones = 5;
        SIMUL.TotalFloorArea = 2294;
        SIMUL.TotalWindowArea = 83.94 ;
        SIMUL.MaxMultiplier =  4; % (1177.02-83.94)/83.94
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype= 'NONE';
                    else
                        SIMUL.setup{i}.surfacetype= 'Mass NonRes Wall Insulation';
                    end
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORE_RETAIL_WALL_EAST_2';
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORE_RETAIL_ROOF';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,FRONT_ENTRY_WALL_SOUTH_GLAZING';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Strip Mall'
        IDFname1 = 'RefBldgStripMallNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_stripmall_new2004_v1-4_7-2\';
        SIMUL.nrZones = 10;
        SIMUL.TotalFloorArea = 2090.32;
        SIMUL.TotalWindowArea = 124.29;
        SIMUL.MaxMultiplier = 4;
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Steel Frame NonRes Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_WALL_1,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_CEILING,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_WALL_1_WINDOW_1,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Primary School'
        IDFname1 = 'RefBldgPrimarySchoolNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_primaryschool_new2004_v1-4_7-2\';
        SIMUL.nrZones = 25;
        SIMUL.TotalFloorArea = 6871;
        SIMUL.TotalWindowArea = 879.18;
        SIMUL.MaxMultiplier = 3; %% TODO
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Steel Frame NonRes Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_WALL_1,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_CEILING,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_WALL_1_WINDOW_1,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
    case 'Secondary School'
        IDFname1 = 'RefBldgSecondarySchoolNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_secondaryschool_new2004_v1-4_7-2\';
        SIMUL.nrZones = 46;
        SIMUL.TotalFloorArea = 19592;
        SIMUL.TotalWindowArea = 2089.2;
        SIMUL.MaxMultiplier = 4.5; %% TODO
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Steel Frame NonRes Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_WALL_1,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_CEILING,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORNER_CLASS_1_POD_2_ZN_1_FLR_1_WALL_1_WINDOW_1,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
    case 'Supermarket'
        IDFname1 = 'RefBldgSuperMarketNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_supermarket_new2004_v1-4_7-2\';
        SIMUL.nrZones = 46;
        SIMUL.TotalFloorArea = 4181;
        SIMUL.TotalWindowArea = 174.7;
        SIMUL.MaxMultiplier = 2; %% (1609.16-174.7)/174.7
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype= 'NONE';
                    else
                        SIMUL.setup{i}.surfacetype= 'Mass NonRes Wall Insulation';
                    end
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,BAKERY_WALL__2,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,SALES_CEILING,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,SALES_WALL_SOUTH_WINDOW,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Quick Service Restaurant'
        IDFname1 = 'RefBldgQuickServiceRestaurantNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_quickservicerestaurant_new2004_v1-4_7-2\';
        SIMUL.nrZones = 3;
        SIMUL.TotalFloorArea = 232;
        SIMUL.TotalWindowArea = 26.03;
        SIMUL.MaxMultiplier = 2; %% TODO
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Wood Frame NonRes Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,DINING_WALL_WEST,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'AtticFloor NonRes Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,EAST-ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,DINING_WALL_EAST_WINDOW,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Full Service Restaurant'
        IDFname1 = 'RefBldgFullServiceRestaurantNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_fullservicerestaurant_new2004_v1-4_7-2\';
        SIMUL.nrZones = 3;
        SIMUL.TotalFloorArea = 511;
        SIMUL.TotalWindowArea = 50.18;
        SIMUL.MaxMultiplier = 2; %% TODO
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Steel Frame NonRes Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,DINING_WALL_WEST,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'AtticFloor NonRes Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,EAST-ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,DINING_WALL_EAST_WINDOW,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Hospital'
        IDFname1 = 'RefBldgHospitalNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_hospital_new2004_v1-4_7-2\';
        SIMUL.nrZones = 55;
        SIMUL.TotalFloorArea = 22422;
        SIMUL.TotalWindowArea = 845.4;
        SIMUL.MaxMultiplier = 4; %% (5786.59-845.4)/845.4
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype= 'NONE';
                    else
                        SIMUL.setup{i}.surfacetype= 'Mass NonRes Wall Insulation';
                    end
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORRIDOR_FLR_1_WALL_NORTH,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,CORRIDOR_FLR_5_CEILING,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,DINING_FLR_5_WALL_SOUTH_WINDOW,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Outpatient Health Care'
        IDFname1 = 'RefBldgOutPatientNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_outpatient_new2004_v1-4_7-2\';
        SIMUL.nrZones = 118;
        SIMUL.TotalFloorArea = 3804;
        SIMUL.TotalWindowArea = 308.3;
        SIMUL.MaxMultiplier = 2; %% TODO
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    SIMUL.setup{1,i}.surfacetype= 'Steel Frame NonRes Wall Insulation';
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,1535AC,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD NonRes Roof Insulation';
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,FLOOR 3 WORK ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,137A56,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
    case 'Large Hotel'
        IDFname1 = 'RefBldgLargeHotelNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_largehotel_new2004_v1-4_7-2\';
        SIMUL.nrZones = 22;
        SIMUL.TotalFloorArea = 11345.29;
        SIMUL.TotalWindowArea = 1214.08;
        SIMUL.MaxMultiplier = 2; %% (4559.59-1214.08)/1214.08
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype = 'NONE';
                        SIMUL.setup{i}.nsurfacetypes = 1;
                    else
                        SIMUL.setup{i}.surfacetype = 'Mass NonRes Wall Insulation';
                        SIMUL.setup{i}.nsurfacetypes = 2;
                    end
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,1535AC,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD Res Roof Insulation';
                    SIMUL.setup{i}.nsurfacetypes = 1;
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,FLOOR 3 WORK ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.nsurfacetypes = length(SIMUL.setup{i}.surfacetype);
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,137A56,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.nsurfacetypes = length(SIMUL.setup{i}.surfacetype);
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.nsurfacetypes = length(SIMUL.setup{i}.surfacetype);
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
        
    case 'Midrise Apartment'
        IDFname1 = 'RefBldgMidriseApartmentNew2004_v1.4_7.2';
        SIMUL.FolderName = 'refbldg_midriseapartment_new2004_v1-4_7-2\';
        SIMUL.nrZones = 22;
        SIMUL.TotalFloorArea = 11345.29;
        SIMUL.TotalWindowArea = 1214.08;
        SIMUL.MaxMultiplier = 2; %% (4559.59-1214.08)/1214.08
        
        %% Properties of the parameters of interest
        for i = 1:nInputs
            switch inputOfInterest{i}
                case 'External Walls U-factor'
                    % CORRESPONDING TO: Walls Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Walls Insulation Thickness';
                    SIMUL.setup{1,i}.section = 'Material';
                    if (isempty(strfind(ClimateZone, '1A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2A'))==0) || ...
                            (isempty(strfind(ClimateZone, '2B'))==0)
                        SIMUL.setup{i}.surfacetype = 'NONE';
                        SIMUL.setup{i}.nsurfacetypes = 1;
                    else
                        SIMUL.setup{i}.surfacetype = 'Mass NonRes Wall Insulation';
                        SIMUL.setup{i}.nsurfacetypes = 2;
                    end
                    SIMUL.setup{1,i}.parameter = 'Thickness';
                    SIMUL.setup{1,i}.nparameter = 1;
                    SIMUL.setup{1,i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionExtWalls;
                    if strcmp(OPTIONS.DistributionExtWalls, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeExtWalls(1) , OPTIONS.RangeExtWalls(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanExtWalls;
                        SIMUL.setup{i}.std = OPTIONS.StdExtWalls;
                    end
                    SIMUL.setup{i}.syntax = '%0.16f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,1535AC,Wall,'; % TODO
                case 'Roof U-factor'
                    % CORRESPONDING TO: Roof Insulation Thickness
                    SIMUL.setup{i}.lev1variable = 'Roof Insulation Thickness';
                    SIMUL.setup{i}.section = 'Material';
                    SIMUL.setup{i}.surfacetype= 'IEAD Res Roof Insulation';
                    SIMUL.setup{i}.nsurfacetypes = 1;
                    SIMUL.setup{i}.parameter = 'Thickness';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Material Parameter';
                    SIMUL.setup{i}.distribution = OPTIONS.DistributionRoof;
                    if strcmp(OPTIONS.DistributionRoof, 'Uniform')
                        SIMUL.setup{i}.range = [OPTIONS.RangeRoof(1) , OPTIONS.RangeRoof(2)];
                    else
                        SIMUL.setup{i}.mean = OPTIONS.MeanRoof;
                        SIMUL.setup{i}.std = OPTIONS.StdRoof;
                    end
                    SIMUL.setup{i}.syntax = '%0.15f';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,FLOOR 3 WORK ROOF,Roof,';
                case 'Glazing U-factor'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.nsurfacetypes = length(SIMUL.setup{i}.surfacetype);
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = 'HeatTransfer_Surface,137A56,Window,';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Glazing SHGC'
                    % CORRESPONDING TO: Type of Glazing
                    SIMUL.setup{i}.lev1variable = 'Type of Glazing';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.nsurfacetypes = length(SIMUL.setup{i}.surfacetype);
                    SIMUL.setup{i}.parameter = 'Construction Name';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration';
                    SIMUL.setup{i}.distribution = 'Discrete uniform selection';
                    SIMUL.setup{i}.syntax = '';
                    SIMUL.setup{i}.samplesurface = '';
                    SIMUL.setup{i}.library = OPTIONS.Library;
                case 'Window-to-floor ratio'
                    % CORRESPONDING TO: Window Size
                    SIMUL.setup{i}.lev1variable = 'Multiplier';
                    SIMUL.setup{i}.section = 'FenestrationSurface:Detailed';
                    SIMUL.setup{i}.surfacetype= 'Window,                  !- Surface Type';
                    SIMUL.setup{i}.nsurfacetypes = length(SIMUL.setup{i}.surfacetype);
                    SIMUL.setup{i}.parameter = 'Multiplier';
                    SIMUL.setup{i}.nparameter = 1;
                    SIMUL.setup{i}.type = 'Surface Configuration Parameter';
                    SIMUL.setup{i}.range = [1 , SIMUL.MaxMultiplier]; %range?
                    SIMUL.setup{i}.distribution = 'Uniform';
                    SIMUL.setup{i}.syntax = '%0.4f';
                    SIMUL.setup{i}.samplesurface = '';
            end
        end
end



SIMUL.building = strcat(IDFname1,IDFname2);


