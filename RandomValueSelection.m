function [selectedValue] = RandomValueSelection(section, parameter, Library);
fulllib = 0;
switch section
    case 'FenestrationSurface:Detailed'
        if strcmp(parameter, 'Construction Name') == 1
            if fulllib
            Library = {'Sgl Clr 3mm',...
                'Sgl Clr 6mm',...
                'Sgl Clr Low Iron 3mm',...
                'Sgl Clr Low Iron 5mm',...
                'Sgl Bronze 3mm',...
                'Sgl Bronze 6mm',...
                'Sgl Green 3mm',...
                'Sgl Green 6mm',...
                'Sgl Grey 3mm',...
                'Sgl Grey 6mm',...
                'Sgl Blue 6mm',...
                'Sgl Ref-A-L Clr 6mm',...
                'Sgl Ref-A-M Clr 6mm',...
                'Sgl Ref-A-H Clr 6mm',...
                'Sgl Ref-A-L Tint 6mm',...
                'Sgl Ref-A-M Tint 6mm',...
                'Sgl Ref-A-H Tint 6mm',...
                'Sgl Ref-B-L Clr 6mm',...
                'Sgl Ref-B-H Clr 6mm',...
                'Sgl Ref-B-L Tint 6mm',...
                'Sgl Ref-B-M Tint 6mm',...
                'Sgl Ref-B-H Tint 6mm',...
                'Sgl Ref-C-L Clr 6mm',...
                'Sgl Ref-C-M Clr 6mm',...
                'Sgl Ref-C-H Clr 6mm',...
                'Sgl Ref-C-L Tint 6mm',...
                'Sgl Ref-C-M Tint 6mm',...
                'Sgl Ref-C-H Tint 6mm',...
                'Sgl Ref-D Clr 6mm',...
                'Sgl Ref-D Tint 6mm',...
                'Sgl LoE (e2=.4) Clr 3mm',...
                'Sgl LoE (e2=.2) Clr 3mm',...
                'Sgl LoE (e2=.2) Clr 6mm',...
                'Sgl Elec Abs Bleached 6mm',...
                'Sgl Elec Abs Colored 6mm',...
                'Sgl Elec Ref Bleached 6mm',...
                'Sgl Elec Ref Colored 6mm',...
                'Dbl Clr 3mm/6mm Air',...
                'Dbl Clr 3mm/13mm Air',...
                'Dbl Clr 3mm/13mm Arg',...
                'Dbl Clr 6mm/6mm Air',...
                'Dbl Clr 6mm/13mm Air',...
                'Dbl Clr 6mm/13mm Arg',...
                'Dbl Clr Low Iron 3mm/6mm Air',...
                'Dbl Clr Low Iron 3mm/13mm Air',...
                'Dbl Clr Low Iron 3mm/13mm Arg',...
                'Dbl Clr Low Iron 5mm/6mm Air',...
                'Dbl Clr Low Iron 5mm/13mm Air',...
                'Dbl Clr Low Iron 5mm/13mm Arg',...
                'Dbl Bronze 3mm/6mm Air',...
                'Dbl Bronze 3mm/13mm Air',...
                'Dbl Bronze 3mm/13mm Arg',...
                'Dbl Bronze 6mm/6mm Air',...
                'Dbl Bronze 6mm/13mm Air',...
                'Dbl Bronze 6mm/13mm Arg',...
                'Dbl Green 3m/6mm Air',...
                'Dbl Green 3mm/13mm Air',...
                'Dbl Green 3mm/13mm Arg',...
                'Dbl Green 6mm/6mm Air',...
                'Dbl Green 6mm/13mm Air',...
                'Dbl Green 6mm/13mm Arg',...
                'Dbl Grey 3mm/6mm Air',...
                'Dbl Grey 3mm/13mm Air',...
                'Dbl Grey 3mm/13mm Arg',...
                'Dbl Grey 6mm/6mm Air',...
                'Dbl Grey 6mm/13mm Air',...
                'Dbl Grey 6mm/13mm Arg',...
                'Dbl Blue 6mm/6mm Air',...
                'Dbl Blue 6mm/13mm Air',...
                'Dbl Blue 6mm/13mm Arg',...
                'Dbl Ref-A-L Clr 6mm/6mm Air',...
                'Dbl Ref-A-L Clr 6mm/13mm Air',...
                'Dbl Ref-A-L Clr 6mm/13mm Arg',...
                'Dbl Ref-A-M Clr 6mm/6mm Air',...
                'Dbl Ref-A-M Clr 6mm/13mm Air',...
                'Dbl Ref-A-M Clr 6mm/13mm Arg',...
                'Dbl Ref-A-H Clr 6mm/6mm Air',...
                'Dbl Ref-A-H 6mm/13mm Air',...
                'Dbl Ref-A-H Clr 6mm/13mm Arg',...
                'Dbl Ref-A-L Tint 6mm/6mm Air',...
                'Dbl Ref-A-L Tint 6mm/13mm Air',...
                'Dbl Ref-A-L Tint 6mm/13mm Arg',...
                'Dbl Ref-A-M Tint 6mm/6mm Air',...
                'Dbl Ref-A-M Tint 6mm/13mm Air',...
                'Dbl Ref-A-M Tint 6mm/13mm Arg',...
                'Dbl Ref-A-H Tint 6mm/6mm Air',...
                'Dbl Ref-A-H Tint 6mm/13mm Air',...
                'Dbl Ref-A-H Tint 6mm/13mm Arg',...
                'Dbl Ref-B-L Clr 6mm/6mm Air',...
                'Dbl Ref-B-L Clr 6mm/13mm Air',...
                'Dbl Ref-B-L Clr 6mm/13mm Arg',...
                'Dbl Ref-B-H Clr 6mm/6mm Air',...
                'Dbl Ref-B-H Clr 6mm/13mm Air',...
                'Dbl Ref-B-H Clr 6mm/13mm Arg',...
                'Dbl Ref-B-L Tint 6mm/6mm Air',...
                'Dbl Ref-B-L Tint 6mm/13mm Air',...
                'Dbl Ref-B-L Tint 6mm/13mm Arg',...
                'Dbl Ref-B-M Tint 6mm/6mm Air',...
                'Dbl Ref-B-M Tint 6mm/13mm Air',...
                'Dbl Ref-B-M Tint 6mm/13mm Arg',...
                'Dbl Ref-B-H Tint 6mm/6mm Air',...
                'Dbl Ref-B-H Tint 6mm/13mm Air',...
                'Dbl Ref B-H Tint 6mm/13mm Arg',...
                'Dbl Ref-C-L Clr 6mm/6mm Air',...
                'Dbl Ref-C-L Clr 6mm/13mm Air',...
                'Dbl Ref-C-L Clr 6mm/13mm Arg',...
                'Dbl Ref-C-M Clr 6mm/6mm Air',...
                'Dbl Ref-C-M Clr 6mm/13mm Air',...
                'Dbl Ref-C-M Clr 6mm/13mm Arg',...
                'Dbl Ref-C-H Clr 6mm/6mm Air',...
                'Dbl Ref-C-H Clr 6mm/13mm Air',...
                'Dbl Ref-C-H Clr 6mm/13mm Arg',...
                'Dbl Ref-C-L Tint 6mm/6mm Air',...
                'Dbl Ref-C-L Tint 6mm/13mm Air',...
                'Dbl Ref-C-L Tint 6mm/13mm Arg',...
                'Dbl Ref-C-M Tint 6mm/6mm Air',...
                'Dbl Ref-C-M Tint 6mm/13mm Air',...
                'Dbl Ref-C-M Tint 6mm/13mm Arg',...
                'Dbl Ref-C-H Tint 6mm/6mm Air',...
                'Dbl Ref-C-H Tint 6mm/13mm Air',...
                'Dbl Ref-C-H Tint 6mm/13mm Arg',...
                'Dbl Ref-D Clr 6mm/6mm Air',...
                'Dbl Ref-D Clr 6mm/13mm Air',...
                'Dbl Ref-D Clr 6mm/13mm Arg',...
                'Dbl Ref-D Tint 6mm/6mm Air',...
                'Dbl Ref-D Tint 6mm/13mm Air',...
                'Dbl Ref-D Tint 6mm/13mm Arg',...
                'Dbl LoE (e2=.4) Clr 3mm/6mm Air',...
                'Dbl LoE (e2=.4) Clr 3mm/13mm Air',...
                'Dbl LoE (e2=.4) Clr 3mm/13mm Arg',...
                'Dbl LoE (e2=.2) Clr 3mm/6mm Air',...
                'Dbl LoE (e2=.2) Clr 3mm/13mm Air',...
                'Dbl LoE (e2=.2) Clr 3mm/13mm Arg',...
                'Dbl LoE (e2=.2) Clr 6mm/6mm Air',...
                'Dbl LoE (e2=.2) Clr 6mm/13mm Air',...
                'Dbl LoE (e2=.2) Clr 6mm/13mm Arg',...
                'Dbl LoE (e2=.1) Clr 3mm/6mm Air',...
                'Dbl LoE (e2=.1) Clr 3mm/13mm Air',...
                'Dbl LoE (e2=.1) Clr 3mm/13mm Arg',...
                'Dbl LoE (e2=.1) Clr 6mm/6mm Air',...
                'Dbl LoE (e2=.1) Clr 6mm/13mm Air',...
                'Dbl LoE (e2=.1) Clr 6mm/13mm Arg',...
                'Dbl LoE (e2=.1) Tint 6mm/6mm Air',...
                'Dbl LoE (e2=.1) Tint 6mm/13mm Air',...
                'Dbl LoE (e2=.1) Tint 6mm/13mm Arg',...
                'Dbl LoE (e3=.1) Clr 3mm/6mm Air',...
                'Dbl LoE (e3=.1) Clr 3mm/13mm Air',...
                'Dbl LoE (e3=.1) Clr 3mm/13mm Arg',...
                'Dbl LoE Spec Sel Clr 3mm/6mm/6mm Air',...
                'Dbl LoE Spec Sel Clr 3mm/13mm/6mm Air',...
                'Dbl LoE Spec Sel Clr 3mm/13mm/6mm Arg',...
                'Dbl LoE Spec Sel Clr 6mm/6mm Air',...
                'Dbl LoE Spec Sel Clr 6mm/13mm Air',...
                'Dbl LoE Spec Sel Clr 6mm/13mm Arg',...
                'Dbl LoE Spec Sel Tint 6mm/6mm Air',...
                'Dbl LoE Spec Sel Tint 6mm/13mm Air',...
                'Dbl LoE Spec Sel Tint 6mm/13mm Arg',...
                'Dbl Elec Abs Bleached 6mm/6mm Air',...
                'Dbl Elec Abs Colored 6mm/6mm Air',...
                'Dbl Elec Abs Bleached 6mm/13mm Air',...
                'Dbl Elec Abs Colored 6mm/13mm Air',...
                'Dbl Elec Abs Bleached 6mm/13mm Arg',...
                'Dbl Elec Abs Colored 6mm/13mm Arg',...
                'Dbl Elec Ref Bleached 6mm/6mm Air',...
                'Dbl Elec Ref Colored 6mm/6mm Air',...
                'Dbl Elec Ref Bleached 6mm/13mm Air',...
                'Dbl Elec Ref Colored 6mm/13mm Air',...
                'Dbl Elec Ref Bleached 6mm/13mm Arg',...
                'Dbl Elec Ref Colored 6mm/13mm Arg',...
                'Dbl LoE Elec Abs Bleached 6mm/6mm Air',...
                'Dbl LoE Elec Abs Colored 6mm/6mm Air',...
                'Dbl LoE Elec Abs Bleached 6mm/13mm Air',...
                'Dbl LoE Elec Abs Colored 6mm/13mm Air',...
                'Dbl LoE Elec Abs Bleached 6mm/13mm Arg',...
                'Dbl LoE Elec Abs Colored 6mm/13mm Arg',...
                'Dbl LoE Elec Ref Bleached 6mm/6mm Air',...
                'Dbl LoE Elec Ref Colored 6mm/6mm Air',...
                'Dbl LoE Elec Ref Bleached 6mm/13mm Air',...
                'Dbl LoE Elec Ref Colored 6mm/13mm Air',...
                'Dbl LoE Elec Ref Bleached 6mm/13mm Arg',...
                'Dbl LoE Elec Ref Colored 6mm/13m Arg',...
                'Trp Clr 3mm/6mm Air',...
                'Trp Clr 3mm/13mm Air',...
                'Trp Clr 3mm/13mm Arg',...
                'Trp LoE (e5=.1) Clr 3mm/6mm Air',...
                'Trp LoE (e5=.1) Clr 3mm/13mm Air',...
                'Trp LoE (e5=.1) Clr 3mm/13mm Arg',...
                'Trp LoE (e2=e5=.1) Clr 3mm/6mm Air',...
                'Trp LoE (e2=e5=.1) Clr 3mm/13mm Air',...
                'Trp LoE (e2=e5=.1) Clr 3mm/13mm Arg',...
                'Trp LoE Film (88) Clr 3mm/6mm Air',...
                'Trp LoE Film (88) Clr 3mm/13mm Air',...
                'Trp LoE Film (77) Clr 3mm/6mm Air',...
                'Trp LoE Film (77) Clr 3mm/13mm Air',...
                'Trp LoE Film (66) Clr 6mm/6mm Air',...
                'Trp LoE Film (66) Clr 6mm/13mm Air',...
                'Trp LoE Film (66) Bronze 6mm/6mm Air',...
                'Trp LoE Film (66) Bronze 6mm/13mm Air',...
                'Trp LoE Film (55) Clr 6mm/6mm Air',...
                'Trp LoE Film (55) Clr 6mm/13m Air',...
                'Trp LoE Film (55) Bronze 6mm/6mm Air',...
                'Trp LoE Film (55) Bronze 6mm/13mm Air',...
                'Trp LoE Film (44) Bronze 6mm/6mm Air',...
                'Trp LoE Film (44) Bronze 6mm/13mm Air',...
                'Trp LoE Film (33) Bronze 6mm/6mm Air',...
                'Trp LoE Film (33) Bronze 6mm/13mm Air',...
                'Quadruple LoE Films (88) 3mm/8mm Krypton'
                };
            end
            nrConstructions = length(Library);
            x = randi(nrConstructions);
            selectedValue = Library(x);
        end
end
end

            
            
            