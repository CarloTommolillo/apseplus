function [] = RunEPlusSimulation(EnergyPlusPath, idfFile, weatherFile, echoFlag)

cmd = [EnergyPlusPath ' ' idfFile ' ' weatherFile];
if echoFlag %&& not(parFlag)
    [status, result] = system([cmd],'-echo');
else
    [status, result] = system([cmd]);
end

%C:\EnergyPlusV8-4-0\RunEplus C:\Users\Carlo\Desktop\EnergyPlusOUTPUT\RefBldgSmallOfficeNew2004_v1.4_7.2_5A_USA_IL_CHICAGO-OHARE_\RefBldgSmallOfficeNew2004_v1.4_7.2_5A_USA_IL_CHICAGO-OHARE USA_IL_Chicago-OHare.Intl.AP.725300_TMY3