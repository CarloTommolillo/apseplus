function [SimOutput, SimInput, initializationTime, countIter] = SimulateWithEnergyPlus(SIMUL, outputOfInterest, outputFolder, inputOfInterest, EnergyPlusPath)
tic;

building = SIMUL.building;
nrZones = SIMUL.nrZones;
version = SIMUL.version;

SimInput = {};
SimOutput = {};

newStr = '';

%% Find material and parameter
[fid, msg] = fopen(strcat(outputFolder,building,'.idf'),'r+');

tline = char(fgetl(fid));

skip_section = zeros(1,length(SIMUL.setup)); % skip = 0 is parameter still unfinished; skip = 1 is parameter finished; skip = 2 is parameter to be modified with same value
newValue =cell(1,length(SIMUL.setup));
countIter = 0;
activityFlag = 0; % if 0, no modification has been performed in the preceding step;
% otherwise, equal to the index of the parameter that has just been modified
check = 1;
while check % LA VARIABILE CHECK � INUTILE!
    countIter = countIter +1;
    skip_parameter = zeros(1,length(SIMUL.setup));
    found_section = 0;
    found_parameter = 0;
    found_surfacetype = 0;
    
    [lastActiveParameter] = max(find(skip_section~=1)); % Last parameter in the list of SIMUL that still need to be considered.
    % Needed because when we reach it, we can leave the current
    % section, since no further modifications have to be performed
    % there.
    
    newStr = sprintf('%s\n%s',newStr,tline);
    if countIter == 1
        [~, newStr] = strtok(newStr,'!');
    end
    tline = fgetl(fid);
    if tline == -1
        break
    end
    
    for i = 1:length(SIMUL.setup) %% For each parameter, repeat...
        nparameter = SIMUL.setup{i}.nparameter;
        %nsurfacetypes = SIMUL.setup{1,i}.nsurfacetypes;
        %found_surfacetype = zeros(1,nsurfacetypes);
        found_surfacetype = 0;
        % Check whether the same parameter has been already modified
        if i > 1
            for j = 1:i-1
                if strcmp(SIMUL.setup{i}.section,SIMUL.setup{j}.section) &&...
                        strcmp(SIMUL.setup{i}.surfacetype,SIMUL.setup{j}.surfacetype) &&...
                        any(any(strcmp(SIMUL.setup{i}.parameter,SIMUL.setup{j}.parameter)))
                    % Already modified
                    skip_section(i) = 1;
                end
            end
        end
        switch skip_section(i)
            case 1
                continue % skip to following parameter. The current parameter
                % doesn't need to be modified anymore in the idf.
            otherwise
                section = SIMUL.setup{i}.section;
                surfacetype = SIMUL.setup{i}.surfacetype;
                type = SIMUL.setup{i}.type;
                parameter = SIMUL.setup{i}.parameter;
                distribution = SIMUL.setup{i}.distribution;
                syntax = SIMUL.setup{i}.syntax;
                lev1variable = SIMUL.setup{i}.lev1variable;
                found_section = (isempty(strfind(tline, section))==0);
                
                % If we just edited a parameter in the same section, we're
                % still inside it!
                if activityFlag ~= 0 && activityFlag ~= i
                    if i > 1 && strcmp(SIMUL.setup{i}.section, SIMUL.setup{activityFlag}.section) &&...
                            strcmp(SIMUL.setup{i}.surfacetype, SIMUL.setup{activityFlag}.surfacetype)
                        found_section = 1;
                        %found_surfacetype = ones(1,nsurfacetypes);
                        found_surfacetype = 1;
                    elseif i > 1 && strcmp(SIMUL.setup{i}.section, SIMUL.setup{activityFlag}.section)
                        found_section = 1;
                        %found_surfacetype = zeros(1,nsurfacetypes);
                        found_surfacetype = 0;
                    end
                end
                if found_section == 1
                    % Once we found the section, we look for
                    % the surface type line if present
                    if strcmp(surfacetype,'NONE') == 0
                        while found_surfacetype == 0
                            newStr = sprintf('%s\n%s',newStr,tline);
                            tline = fgetl(fid);
                            if tline == -1
                                error('Didnt find %s in the IDF file', surfacetype);
                                break
                            end
                            found_surfacetype = (isempty(strfind(tline, surfacetype))==0);
                        end
                    else
                        skip_parameter(i) = nparameter;
                    end
                    % Once we found the section (and eventually the surfacetype), we look for the parameter line
                    while skip_parameter(i) < nparameter
                        skip_parameter(i) = skip_parameter(i) + 1;
                        found_parameter = 0;
                        while found_parameter == 0
                            newStr = sprintf('%s\n%s',newStr,tline);
                            tline = fgetl(fid);
                            activityFlag = 0;
                            if tline == -1
                                if nparameter > 1
                                    error('Didnt find %s in the IDF file', temp);
                                else
                                    error('Didnt find %s in the IDF file', parameter);
                                end
                            end
                            if nparameter > 1
                                temp = parameter{1,skip_parameter(i)};
                                found_parameter = (isempty(strfind(tline, temp))==0);
                            else
                                found_parameter = (isempty(strfind(tline, parameter))==0);
                            end
                        end
                        if i == lastActiveParameter
                            activityFlag = 0;
                        else
                            activityFlag = i;
                        end
                        
                        switch type
                            case 'Material Parameter'
                                %% Generate new value at random according to distribution
                                if skip_section(i) == 0
                                    switch distribution
                                        case 'Uniform'
                                            range = SIMUL.setup{i}.range;
                                            newValue{i} = range(1) + (range(2)-range(1))*rand;
                                        case 'Gaussian'
                                            newValue{i} = normrnd(SIMUL.setup{i}.mean, SIMUL.setup{i}.std);
                                    end
                                end
                                %% Read current value and replace it
                                [value, ~, ~,~] = strread(tline, ' %f, %s %s %s');
                                tline = regexprep(tline, sprintf(syntax,value),sprintf(syntax,newValue{i}));
                                newStr = sprintf('%s\n%s',newStr,tline);
                                tline = fgetl(fid);
                                SimInput = setfield(SimInput, lev1variable(~isspace(lev1variable)), newValue{i});
                                skip_section(i) = 1; % material parameter only need one modification in the idf file
                                
                                %if (strcmp(surfacetype, 'Mass NonRes Wall Insulation')||strcmp(surfacetype, 'Steel Frame NonRes Wall Insulation')) && strcmp(parameter, 'Thickness') && sum(tline ~= -1) ~= 0
                                %    [value, ~, ~,~] = strread(tline, ' %f, %s %s %s');
                                %    tline = regexprep(tline, sprintf('%0.16f',value),sprintf('%0.16f',newValue{i}));
                                %    newStr = sprintf('%s\n%s',newStr,tline);
                                %    tline = fgetl(fid);
                                %    SimInput,WallsInsulationThickness = newValue{i};
                                %    skip_section(i) = 1; % material parameter only need one modification in the idf file
                                %end
                                %if strcmp(surfacetype, 'AtticFloor NonRes Insulation') && strcmp(parameter, 'Thickness') && sum(tline ~= -1) ~= 0
                                %    [value, ~, ~,~] = strread(tline, ' %f, %s %s %s');
                                %    tline = regexprep(tline, sprintf('%0.15f',value),sprintf('%0.15f',newValue{i}));
                                %    newStr = sprintf('%s\n%s',newStr,tline);
                                %    tline = fgetl(fid);
                                %    SimInput.AtticFloorInsulationThickness = newValue{i};
                                %    skip_section(i) = 1; % material parameter only need one modification in the idf file
                                %end
                                
                            case 'Surface Configuration'
                                %% Generate new value at random according to distribution
                                if skip_section(i) == 0
                                    switch distribution
                                        case 'Discrete uniform selection'
                                            % Select the value at random from the library
                                            [newValue{i}] = RandomValueSelection(section, parameter, SIMUL.setup{i}.library);
                                            newValue{i} = char(newValue{i});
                                    end
                                end
                                %% Read current value and replace it
                                if strcmp(parameter, 'Construction Name')
                                    [value, ~] = strtok(tline,',');
                                    value = value(5:end);
                                    if length(newValue{i}) > 24
                                        tline = ['    ',newValue{i},',  ','!- Construction Name'];
                                        %tline = char(tline);
                                    else
                                        nSpaces = 25-length(newValue{i})-1;
                                        tline = ['    ',newValue{i},','];
                                        for l = 1:nSpaces
                                            tline = [tline,' '];
                                            %tline = char(tline);
                                        end
                                        tline = [tline,'!- Construction Name'];
                                    end
                                end
                                newStr = sprintf('%s\n%s',newStr,tline);
                                tline = fgetl(fid);
                                SimInput = setfield(SimInput, lev1variable(~isspace(lev1variable)), newValue{i});
                                skip_section(i) = 2; % surface configurations need many modifications in the idf file, but with the same value
                                
                            case 'Surface Configuration Parameter'
                                %% Generate new value at random according to distribution
                                if skip_section(i) == 0
                                    switch distribution
                                        case 'Uniform'
                                            range = SIMUL.setup{i}.range;
                                            newValue{i} = range(:,1) + (range(:,2)-range(:,1)).*rand(size(range,1),1); % crea vettore di dim 2 con altezza inizioe  fine finestra
                                    end
                                end
                                %% Read current value and replace it
                                if strcmp(parameter, 'Multiplier')
                                    [value, ~, ~, ~] = strread(tline, ' %f, %s %s %s');
                                    tline = regexprep(tline, sprintf(syntax,value),sprintf(syntax,newValue{i}));
                                    SimInput = setfield(SimInput, lev1variable(~isspace(lev1variable)), newValue{i});
                                    SimInput.WFR = (SIMUL.TotalWindowArea * SimInput.Multiplier)/SIMUL.TotalFloorArea;
                                else
                                    temp = parameter{1,skip_parameter(i)};
                                    if strcmp(temp, 'Vertex 1 Z-coordinate') ||...
                                            strcmp(temp, 'Vertex 2 Z-coordinate') ||...
                                            strcmp(temp, 'Vertex 3 Z-coordinate')
                                        [value, ~, ~, ~, ~, ~] = strread(tline, ' %f, %s %s %s %s %s');
                                        tline = regexprep(tline, sprintf('%0.4f',value),sprintf('%0.4f',newValue{i}(parameter{2,skip_parameter(i)})));
                                    elseif strcmp(temp, 'Vertex 4 Z-coordinate')
                                        [value, ~, ~, ~, ~, ~] = strread(tline, ' %f; %s %s %s %s %s');
                                        tline = regexprep(tline, sprintf('%0.4f',value),sprintf('%0.4f',newValue{i}(parameter{2,skip_parameter(i)})));
                                    end
                                    SimInput.WindowHeight = newValue{i};
                                end
                                newStr = sprintf('%s\n%s',newStr,tline);
                                tline = fgetl(fid);
                                
                                skip_section(i) = 2;
                        end
                    end
                    break
                end
        end
    end
end
fclose(fid);

% Modify .idf file
cd(outputFolder)
fid2 = fopen(strcat(outputFolder,building, '.idf'),'wt');
fprintf(fid2,'%s',newStr);
%fwrite(fid2,newStr)
fclose(fid2) ;
disp('IDF FILE GENERATED');

initializationTime = toc;


%% RUN SIMULATION WITH ENERGY-PLUS
%EnergyPlusPath = 'C:\EnergyPlusV8-4-0\RunEplus';
disp('-----------------------------------------------------------------');
disp('SIMULATION STARTED')
EnergyPlusPath = [EnergyPlusPath 'RunEPlus'];
RunEPlusSimulation(EnergyPlusPath, strcat(outputFolder, building), SIMUL.weatherfile, SIMUL.echoFlag);
disp('SIMULATION FINISHED')
disp('-----------------------------------------------------------------');

%% SAVE INPUT
nInputs = length(inputOfInterest);
for i = 1:nInputs
    switch inputOfInterest{i}
        case 'External Walls U-factor'
            switch version
                case '8.4.0'
                    fid3 = fopen(strcat(outputFolder,'eplusout.eio'),'r+');
                case '8.5.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
                case '8.6.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
            end
            found_HeatTransfer_Surface = 0;
            while found_HeatTransfer_Surface == 0
                tline = fgetl(fid3);
                found_HeatTransfer_Surface = (isempty(strfind(tline, SIMUL.setup{i}.samplesurface))==0);
            end
            for k=1:6
                [Lev2Value, tline] = strtok(tline,',');
            end
            SimInput.ExtWallsUfactor = str2num(Lev2Value);
            fclose(fid3);
            %disp('External Walls U-factor DONE');
            
        case 'Roof U-factor'
            switch version
                case '8.4.0'
                    fid3 = fopen(strcat(outputFolder,'eplusout.eio'),'r+');
                case '8.5.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
                case '8.6.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
            end
            found_HeatTransfer_Surface = 0;
            while found_HeatTransfer_Surface == 0
                tline = fgetl(fid3);
                found_HeatTransfer_Surface = (isempty(strfind(tline, SIMUL.setup{i}.samplesurface))==0);
            end
            for k=1:6
                [Lev2Value, tline] = strtok(tline,',');
            end
            SimInput.RoofUfactor = str2num(Lev2Value);
            fclose(fid3);
            %disp('Roof U-factor DONE');
            
        case 'Glazing SHGC'
            switch version
                case '8.4.0'
                    fid3 = fopen(strcat(outputFolder,'eplusout.eio'),'r+');
                case '8.5.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
                case '8.6.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
            end
            found_HeatTransfer_Surface = 0;
            for k = 1:length(SIMUL.setup)
                if any(strcmp(SIMUL.setup{k}.parameter, 'Construction Name'))
                    desiredIndex = k;
                    break
                end
            end
            while found_HeatTransfer_Surface == 0
                tline = fgetl(fid3);
                found_HeatTransfer_Surface = (isempty(strfind(tline, upper(newValue{desiredIndex})))==0);
                if sum(tline == -1) == 1 && found_HeatTransfer_Surface == 0
                    error('Glazing SHGC not found.')
                end
            end
            for k=1:7
                [Lev2Value, tline] = strtok(tline,',');
            end
            SimInput.GlazingSHGC = str2num(Lev2Value);
            fclose(fid3);
            %disp('Glazing SHGC DONE');
            
            
        case 'Glazing U-factor'
            switch version
                case '8.4.0'
                    fid3 = fopen(strcat(outputFolder,'eplusout.eio'),'r+');
                case '8.5.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
                case '8.6.0'
                    fid3 = fopen(strcat(outputFolder, building, '.eio'),'r+');
            end
            found_HeatTransfer_Surface = 0;
            while found_HeatTransfer_Surface == 0
                tline = fgetl(fid3);
                found_HeatTransfer_Surface = (isempty(strfind(tline, SIMUL.setup{i}.samplesurface))==0);
            end
            for k=1:7
                [Lev2Value, tline] = strtok(tline,',');
            end
            SimInput.GlazingUfactor = str2num(Lev2Value);
            fclose(fid3);
            %disp('Glazing U-factor DONE');
    end
end

%% SAVE OUTPUT
% NOTE: Facility = Building + HVAC + Plant + Exterior
nOutputs = length(outputOfInterest);

%% Find row identifiers for all outputs
switch version
    case '8.4.0'
        fid3 = fopen(strcat(outputFolder,'eplusout.mtr'),'r+');
    case '8.5.0'
        fid3 = fopen(strcat(outputFolder, building, '.mtr'),'r+');
    case '8.6.0'
        fid3 = fopen(strcat(outputFolder, building, '.mtr'),'r+');
end
found_identifiers = zeros(1,nOutputs);
rowIdentifiers = cell(1, nOutputs);
tline = fgetl(fid3);
while sum(found_identifiers) < nOutputs && sum(tline ~= -1) > 0
    found_identifier = 0;
    for i = 1:nOutputs
        switch outputOfInterest{i}
            case 'Daily Electricity Facility'
                found_identifier = (isempty(strfind(tline,  'Electricity:Facility [J] !Daily'))==0);
            case 'Monthly Electricity Facility'
                found_identifier = (isempty(strfind(tline,  'Electricity:Facility [J] !Monthly'))==0);
            case 'Electricity Facility'
                found_identifier = (isempty(strfind(tline, 'Electricity:Facility [J] !RunPeriod'))==0);
                
            case 'Daily Gas Facility'
                found_identifier = (isempty(strfind(tline, 'Gas:Facility [J] !Daily'))==0);
            case 'Monthly Gas Facility'
                found_identifier = (isempty(strfind(tline, 'Gas:Facility [J] !Monthly'))==0);
            case 'Gas Facility'
                found_identifier = (isempty(strfind(tline, 'Gas:Facility [J] !RunPeriod'))==0);
                
                
            case 'Daily Electricity Building'
                found_identifier = (isempty(strfind(tline, 'Electricity:Building [J] !Daily'))==0);
            case 'Monthly Electricity Building'
                found_identifier = (isempty(strfind(tline, 'Electricity:Building [J] !Monthly'))==0);
            case 'Electricity Building'
                found_identifier = (isempty(strfind(tline, 'Electricity:Building [J] !RunPeriod'))==0);
                
            case 'Daily Gas Building'
                found_identifier = (isempty(strfind(tline, 'Gas:Building [J] !Daily'))==0);
            case 'Monthly Gas Building'
                found_identifier = (isempty(strfind(tline, 'Gas:Building [J] !Monthly'))==0);
            case 'Gas Building'
                found_identifier = (isempty(strfind(tline, 'Gas:Building [J] !RunPeriod'))==0);
                
            case 'Daily Electricity HVAC'
                found_identifier = (isempty(strfind(tline, 'Electricity:HVAC [J] !Daily'))==0);
            case 'Monthly Electricity HVAC'
                found_identifier = (isempty(strfind(tline, 'Electricity:HVAC [J] !Monthly'))==0);
            case 'Electricity HVAC'
                found_identifier = (isempty(strfind(tline, 'Electricity:HVAC [J] !RunPeriod'))==0);
                
            case 'Daily Gas HVAC'
                found_identifier = (isempty(strfind(tline, 'Gas:HVAC [J] !Daily'))==0);
            case 'Monthly Gas HVAC'
                found_identifier = (isempty(strfind(tline, 'Gas:HVAC [J] !Monthly'))==0);
            case 'Gas HVAC'
                found_identifier = (isempty(strfind(tline, 'Gas:HVAC [J] !RunPeriod'))==0);
                
            case 'Daily Electricity Plant'
                found_identifier = (isempty(strfind(tline, 'Electricity:Plant [J] !Daily'))==0);
            case 'Monthly Electricity Plant'
                found_identifier = (isempty(strfind(tline, 'Electricity:Plant [J] !Monthly'))==0);
            case 'Electricity Plant'
                found_identifier = (isempty(strfind(tline, 'Electricity:Plant [J] !RunPeriod'))==0);
                
            case 'Daily Gas Plant'
                found_identifier = (isempty(strfind(tline, 'Gas:Plant [J] !Daily'))==0);
            case 'Monthly Gas Plant'
                found_identifier = (isempty(strfind(tline, 'Gas:Plant [J] !Monthly'))==0);
            case 'Gas Plant'
                found_identifier = (isempty(strfind(tline, 'Gas:Plant [J] !RunPeriod'))==0);
        end
        if found_identifier
            found_identifiers(i) = 1;
            [rowIdentifiers{i}] = strtok(tline,',');
            rowIdentifiers{i} = strcat(rowIdentifiers{i},',');
            break
        end
    end
    tline = fgets(fid3);
end
SimOutput = {};

if tline == -1
    fclose(fid);
    found_identifiers = logical(found_identifiers);
    not_found_identifiers = [];
    for jj = 1:length(found_identifiers)
        if not(found_identifiers(jj)) == 1
            not_found_identifiers{end+1} = outputOfInterest{jj};
            SimOutput = setfield(SimOutput, outputOfInterest{jj}(~isspace(outputOfInterest{jj})), 'NOT FOUND');
        end
    end
    %not_found_identifiers = outputOfInterest{not(found_identifiers)};
    warning(sprintf('%s\n', 'OUTPUTS NOT FOUND IN .mtr FILE: ', '------------------------', not_found_identifiers{:}, '------------------------'));
end


%% Read all entries of output file corresponding to the output identifier
switch version
    case '8.4.0'
        fid = fopen(strcat(outputFolder,'eplusout.mtr'),'r+');
    case '8.5.0'
        fid = fopen(strcat(outputFolder, building, '.mtr'),'r+');
    case '8.6.0'
        fid = fopen(strcat(outputFolder, building, '.mtr'),'r+');
end
tline = fgets(fid);
while isempty(strfind(tline,  'End of Data Dictionary'))
    tline = fgets(fid);
end
while tline ~= -1
    tline = fgetl(fid);
    [inizio] = strtok(tline,',');
    inizio = strcat(inizio,',');
    for i = 1:nOutputs
        %if isempty(strfind(inizio, rowIdentifiers{i}))==0
        if strfind(inizio, rowIdentifiers{i}) == 1
            % The row corresponds to some output of interest. Read it!
            for k=1:2
                [value, tline] = strtok(tline,',');
            end
            value = str2num(value);
            if isempty(strfind(outputOfInterest{i}, 'Daily'))==0
                if isfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})))
                    % The field already exists. Append
                    field = getfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})));
                    field = [field; value];
                    SimOutput = setfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})), field);
                else
                    SimOutput = setfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})), value);
                end
            else if isempty(strfind(outputOfInterest{i}, 'Monthly'))==0
                    if isfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})))
                        % The field already exists. Append
                        field = getfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})));
                        field = [field; value];
                        SimOutput = setfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})), field);
                    else
                        SimOutput = setfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})), value);
                    end
                else
                    SimOutput = setfield(SimOutput, outputOfInterest{i}(~isspace(outputOfInterest{i})), value);
                end
            end
        end
    end
end
fclose(fid);





