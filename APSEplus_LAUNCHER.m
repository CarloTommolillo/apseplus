APSEplusFolder = pwd;
addpath(APSEplusFolder);

%Building = 'Large Office';
% 'Medium Office';
% 'Small Office';
% 'Warehouse';
% 'Stand-alone Retail';
% 'Strip Mall';
% 'Primary School';
% 'Secondary School';
% 'Supermarket';
% 'Quick Service Restaurant';
% 'Full Service Restaurant';
% 'Hospital';
% 'Outpatient Health Care';
% 'Small Hotel';
% 'Large Hotel';
% 'Midrise Apartment';

%ClimateZone = 'Miami';
% 'Houston';
% 'Phoenix';
% 'Altanta';
% 'Los Angeles';
% 'Las Vegas';
% 'San Francisco';
% 'Baltimore';
% 'Albuquerque';
% 'Seattle';
% 'Chicago';
% 'Boulder';
% 'Minneapolis';
% 'Helena';
% 'Duluth';
% 'Fairbanks';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SIMULATION SETUP
SIMUL = SimulationProperties(Building, ClimateZone, inputOfInterest, OPTIONS);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
APSEplusFolder = pwd;

%% CHECK INPUT FILES
CHECK_IDF_FILE;
CHECK_WEATHER_FILE;


%% PREPROCESSING IDF FILE
% Insert outputs of interest according to chosen granularity
%        types of glazing
%        change solar distribution (set to 'MinimalShadowing')
testName = strcat(SIMUL.building,'_');
OutputFolder_test = strcat(OutputFolder, testName,'\');
mkdir(OutputFolder_test);
copyfile(strcat(InputFolder, SIMUL.FolderName, SIMUL.building, '.idf'), OutputFolder_test);
PREPROCESSING_IDF_FILE;


initializationTime = zeros(1,nrSim);
countIter = zeros(1,nrSim);

err_count = 0;
numCores = 4;
tic
%if parFlag
%    matlabpool('local', num2str(numCores))
%    parfor i = 1:nrSim
%            SimOutput{i} = [];
%            SimInput{i} = [];
%            w = getCurrentWorker;
%            temporaryFolder = fullfile(tempdir, ['temporaryFolder',num2str(w.ProcessId),'\']);
%            disp(temporaryFolder)
%            mkdir(temporaryFolder);
%            copyfile(strcat(OutputFolder_test, SIMUL.building, '.idf'), temporaryFolder);
%            %% SIMULATE
%            [SimOutput{i}, SimInput{i}, initializationTime(i), countIter(i)] = SimulateWithEnergyPlus(SIMUL, outputOfInterest, temporaryFolder, inputOfInterest, strcat(EnergyPlusPath,'RunEplus'), parFlag);
%    end
%    matlabpool close
%else
for i = 1:nrSim
    try
        clc
        disp('-----------------------------------------------------------------');
        disp('-----------------------------------------------------------------');
        disp(['START SIMULATION ' num2str(i)]);
        disp(['ABORTED SIMULATIONS ' num2str(err_count)]);
        %if KeepAllIDF
        %    copyfile(strcat(outputMainFolder, SIMUL.building, '.idf'), OutputFolder)
        %end
        SimOutput{i} = [];
        SimInput{i} = [];
        
        %% SIMULATE
        [SimOutput{i}, SimInput{i}, initializationTime(i), countIter(i)] = SimulateWithEnergyPlus(SIMUL, outputOfInterest, OutputFolder_test, inputOfInterest, EnergyPlusPath);
        if KeepAllIDF
            % Rename IDF file
            cd(OutputFolder)
            movefile(strcat(SIMUL.building,'.idf'),strcat(SIMUL.building,'_',num2str(i),'.idf'));
        end
        disp(['END SIMULATION ' num2str(i)]);
        fclose('all')
    catch
        err_count = err_count + 1;
    end
end
time = toc;
save(strcat('RESULTS', num2str(nrSim), Building, ClimateZone, '.mat'));

if DoPlot
    PlotSimulationResults(SimOutput, SimInput, outputOfInterest, inputOfInterest, SIMUL);
end

cd(APSEplusFolder)

