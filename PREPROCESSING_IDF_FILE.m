%% PREPROCESSING IDF FILE
% Insert outputs of interest according to chosen granularity
%        types of glazing
%        change solar distribution (set to 'MinimalShadowing')

%copyfile(strcat(InputFolder, FolderName, SIMUL.building, '.idf'), OutputFolder);

%fid5 = fopen(strcat(InputFolder, SIMUL.FolderName, SIMUL.building, '.idf'),'r+');
fid5 = fopen(strcat(OutputFolder_test, SIMUL.building, '.idf'),'r+');
newStr = '';
tline5 = fgetl(fid5);
%newStr = sprintf('%s\n%s',newStr,tline5);
found_SolarDistribution = 0;
found_WindowMaterial = 0;
found_LoadsConvergenceTolerance = 0;
check = 0;
doneFlag_WindowMaterial = 0;
doneFlag_GlazingConstruction = 0;

while sum((tline5 ~= -1)) || isempty(tline5)
    
    % Does the current line need to be modified? check!
    found_LoadsConvergenceTolerance = (isempty(strfind(tline5, '!- Loads Convergence Tolerance Value'))==0);
    found_SolarDistribution = (isempty(strfind(tline5, '!- Solar Distribution'))==0);
    found_WindowMaterial = (isempty(strfind(tline5, 'WindowMaterial:SimpleGlazingSystem,'))==0);
    found_GlazingConstruction = (isempty(strfind(tline5, '! ***COMMON CONSTRUCTIONS AND MATERIALS***'))==0);
    found_OutputMeter1 = (isempty(strfind(tline5, '!-   ===========  ALL OBJECTS IN CLASS: OUTPUT:METER ==========='))==0);
    found_OutputMeter2 = (isempty(strfind(tline5, 'Output:Meter'))==0);
    
    if found_LoadsConvergenceTolerance ~= 0
        myStr = '    0.0400,                  !- Loads Convergence Tolerance Value';
        newStr = sprintf('%s\n%s', newStr, myStr);
        tline5 = fgetl(fid5);
    elseif found_SolarDistribution ~= 0
        newStr = sprintf('%s\n%s', newStr,'    MinimalShadowing,        !- Solar Distribution');
        found_SolarDistribution = 0;
        check = check +1;
        tline5 = fgetl(fid5);
    elseif found_WindowMaterial ~= 0 && doneFlag_WindowMaterial == 0
        newStr = sprintf('%s\n%s', newStr, tline5);
        while isempty(tline5) == 0
            tline5 = fgetl(fid5);
            newStr = sprintf('%s\n%s', newStr, tline5);
        end
        fid7 = fopen(strcat(InputFolder,'WindowMaterial.txt'),'r+');
        tline7 = fgetl(fid7);
        while sum((tline7 ~= -1)) || isempty(tline7)
            newStr = sprintf('%s\n%s', newStr, tline7);
            tline7 = fgetl(fid7);
        end
        doneFlag_WindowMaterial = 1;
        fclose(fid7);
        found_WindowMaterial = 0;
        tline5 = fgetl(fid5);
    elseif found_GlazingConstruction && doneFlag_GlazingConstruction == 0
        newStr = sprintf('%s\n%s', newStr, tline5);
        tline5 = fgetl(fid5);
        fid7 = fopen(strcat(InputFolder,'GlazingConstruction.txt'),'r+');
        tline7 = fgetl(fid7);
        while sum((tline7 ~= -1)) || isempty(tline7)
            newStr = sprintf('%s\n%s', newStr, tline7);
            tline7 = fgetl(fid7);
        end
        doneFlag_GlazingConstruction = 1;
        fclose(fid7);
        found_GlazingConstruction = 0;
        tline5 = fgetl(fid5);
    elseif found_OutputMeter1
        %% In some idf files output have this structure
        newStr = sprintf('%s\n%s', newStr, tline5); % Save separator line ===========  ALL OBJECTS IN CLASS: OUTPUT:METER ===========
        
        nrOutputs = length(outputOfInterest);
        for j = 1:nrOutputs
            switch outputOfInterest{j}
                case 'Daily Electricity Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Facility,    !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Facility,    !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Facility,    !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Facility,            !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Facility,            !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Facility,            !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Electricity Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Building,    !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Building,    !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Building,    !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Building,            !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Building,            !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Building,            !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Electricity HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:HVAC,        !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:HVAC,        !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:HVAC,        !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:HVAC,                !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:HVAC,                !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:HVAC,                !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Electricity Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Plant,       !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Plant,       !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Electricity:Plant,       !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Plant,               !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Daily;                   !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Plant,               !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    Monthly;                 !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,');
                    newStr = sprintf('%s\n%s', newStr, '    Gas:Plant,               !- Name');
                    newStr = sprintf('%s\n%s', newStr, '    RunPeriod;               !- Reporting Frequency');
                    newStr = sprintf('%s\n%s', newStr, '');
            end
        end
        tline5 = fgetl(fid5);
        break
        
    elseif found_OutputMeter2
        disp('entra in metodo 2');
        nrOutputs = length(outputOfInterest);
        for j = 1:nrOutputs
            switch outputOfInterest{j}
                case 'Daily Electricity Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Facility,Daily;');
                    newStr
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Facility,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Facility,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Facility,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Facility,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas Facility'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Facility,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Electricity Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Building,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Building,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Building,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Building,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Building,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas Building'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Building,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Electricity HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:HVAC,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:HVAC,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:HVAC,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:HVAC,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:HVAC,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas HVAC'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:HVAC,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Electricity Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Plant,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Electricity Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Plant,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Electricity Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Electricity:Plant,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
                    
                case 'Daily Gas Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Plant,Daily;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Monthly Gas Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Plant,Monthly;');
                    newStr = sprintf('%s\n%s', newStr, '');
                case 'Gas Plant'
                    newStr = sprintf('%s\n%s', newStr, 'Output:Meter,Gas:Plant,RunPeriod;');
                    newStr = sprintf('%s\n%s', newStr, '');
            end
        end
        break
        tline5 = fgetl(fid5);
    else
        newStr = sprintf('%s\n%s', newStr, tline5);
        tline5 = fgetl(fid5);
    end
end

fclose(fid5);

cd(OutputFolder)
fid6 = fopen(strcat(OutputFolder_test, SIMUL.building, '.idf'),'wt');
newStr = sprintf('%s\n%s', newStr, 'Output:Constructions,Constructions;');
newStr = sprintf('%s\n%s', newStr, '');
newStr = sprintf('%s\n%s', newStr, 'Output:Surfaces:List,Details;');
fprintf(fid6,'%s',newStr);
fclose(fid6);
