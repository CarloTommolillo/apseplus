function [Building, ClimateZone] = APSEplus(varargin)
% APSEplus MATLAB code for APSEplus.fig
%      APSEplus, by itself, creates a new APSEplus or raises the existing
%      singleton*.
%
%      H = APSEplus returns the handle to a new APSEplus or the handle to
%      the existing singleton*.
%
%      APSEplus('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in APSEplus.M with the given input arguments.
%
%      APSEplus('Property','Value',...) creates a new APSEplus or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before provaGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to provaGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help APSEplus

% Last Modified by GUIDE v2.5 15-Dec-2016 07:43:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @provaGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @provaGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before APSEplus is made visible.
function provaGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to APSEplus (see VARARGIN)



% Choose default command line output for APSEplus
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes APSEplus wait for user response (see UIRESUME)
% uiwait(handles.SimulationsGUI);


% --- Outputs from this function are returned to the command line.
function varargout = provaGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

str = get(handles.popupmenu1,'String');
val = get(handles.popupmenu1,'Value');

Building = str{val};

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

str = get(handles.popupmenu2,'String');
val = get(handles.popupmenu2,'Value');

ClimateZone = str{val};

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ElectricityFacility.
function ElectricityFacility_Callback(hObject, eventdata, handles)
% hObject    handle to ElectricityFacility (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ElectricityFacility


% --- Executes on button press in GasFacility.
function GasFacility_Callback(hObject, eventdata, handles)
% hObject    handle to GasFacility (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of GasFacility


% --- Executes on button press in ElectricityBuilding.
function ElectricityBuilding_Callback(hObject, eventdata, handles)
% hObject    handle to ElectricityBuilding (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ElectricityBuilding


% --- Executes on button press in GasBuilding.
function GasBuilding_Callback(hObject, eventdata, handles)
% hObject    handle to GasBuilding (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of GasBuilding


% --- Executes on button press in ElectricityHVAC.
function ElectricityHVAC_Callback(hObject, eventdata, handles)
% hObject    handle to ElectricityHVAC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ElectricityHVAC


% --- Executes on button press in GasHVAC.
function GasHVAC_Callback(hObject, eventdata, handles)
% hObject    handle to GasHVAC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of GasHVAC


% --- Executes on button press in ElectricityPlant.
function ElectricityPlant_Callback(hObject, eventdata, handles)
% hObject    handle to ElectricityPlant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ElectricityPlant


% --- Executes on button press in ExtWallsUfactor.
function ExtWallsUfactor_Callback(hObject, eventdata, handles)
% hObject    handle to ExtWallsUfactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ExtWallsUfactor


% --- Executes on button press in RoofUfactor.
function RoofUfactor_Callback(hObject, eventdata, handles)
% hObject    handle to RoofUfactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RoofUfactor


% --- Executes on button press in GlazingUfactor.
function GlazingUfactor_Callback(hObject, eventdata, handles)
% hObject    handle to GlazingUfactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of GlazingUfactor


% --- Executes on button press in GlazingSHGC.
function GlazingSHGC_Callback(hObject, eventdata, handles)
% hObject    handle to GlazingSHGC (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of GlazingSHGC


% --- Executes on button press in WTF.
function WTF_Callback(hObject, eventdata, handles)
% hObject    handle to WTF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of WTF


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7


% --- Executes on button press in radiobutton22.
function radiobutton22_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton22



function nrSim_Callback(hObject, eventdata, handles)
% hObject    handle to nrSim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nrSim as text
%        str2double(get(hObject,'String')) returns contents of nrSim as a double


% --- Executes during object creation, after setting all properties.
function nrSim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nrSim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in GasPlant.
function GasPlant_Callback(hObject, eventdata, handles)
% hObject    handle to GasPlant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of GasPlant


% --- Executes on button press in Daily.
function Daily_Callback(hObject, eventdata, handles)
% hObject    handle to Daily (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Daily


% --- Executes on button press in Monthly.
function Monthly_Callback(hObject, eventdata, handles)
% hObject    handle to Monthly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Monthly


% --- Executes on button press in RunPeriod.
function RunPeriod_Callback(hObject, eventdata, handles)
% hObject    handle to RunPeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RunPeriod


% --- Executes on button press in DoPlot.
function DoPlot_Callback(hObject, eventdata, handles)
% hObject    handle to DoPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DoPlot


function InputFolder_Callback(hObject, eventdata, handles)
% hObject    handle to InputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of InputFolder as text
%        str2double(get(hObject,'String')) returns contents of InputFolder as a double


% --- Executes during object creation, after setting all properties.
function InputFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function OutputFolder_Callback(hObject, eventdata, handles)
% hObject    handle to OutputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of OutputFolder as text
%        str2double(get(hObject,'String')) returns contents of OutputFolder as a double


% --- Executes during object creation, after setting all properties.
function OutputFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OutputFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function [Building, ClimateZone] = pushbutton1_Callback(hObject, eventdata, handles)
clc
SimulationsGUI = findobj('Tag','SimulationsGUI');
SimGUI_handle = guidata(SimulationsGUI);
if isfield(SimGUI_handle, 'OPTIONS')
    OPTIONS = SimGUI_handle.OPTIONS;
else
    % Default settings
    OPTIONS.Library = {'Sgl Clr 3mm',...
                'Sgl Clr 6mm',...
                'Sgl Clr Low Iron 3mm',...
                'Sgl Clr Low Iron 5mm',...
                'Sgl Bronze 3mm',...
                'Sgl Bronze 6mm',...
                'Sgl Green 3mm',...
                'Sgl Green 6mm',...
                'Sgl Grey 3mm',...
                'Sgl Grey 6mm',...
                'Sgl Blue 6mm',...
                'Sgl Ref-A-L Clr 6mm',...
                'Sgl Ref-A-M Clr 6mm',...
                'Sgl Ref-A-H Clr 6mm',...
                'Sgl Ref-A-L Tint 6mm',...
                'Sgl Ref-A-M Tint 6mm',...
                'Sgl Ref-A-H Tint 6mm',...
                'Sgl Ref-B-L Clr 6mm',...
                'Sgl Ref-B-H Clr 6mm',...
                'Sgl Ref-B-L Tint 6mm',...
                'Sgl Ref-B-M Tint 6mm',...
                'Sgl Ref-B-H Tint 6mm',...
                'Sgl Ref-C-L Clr 6mm',...
                'Sgl Ref-C-M Clr 6mm',...
                'Sgl Ref-C-H Clr 6mm',...
                'Sgl Ref-C-L Tint 6mm',...
                'Sgl Ref-C-M Tint 6mm',...
                'Sgl Ref-C-H Tint 6mm',...
                'Sgl Ref-D Clr 6mm',...
                'Sgl Ref-D Tint 6mm',...
                'Sgl LoE (e2=.4) Clr 3mm',...
                'Sgl LoE (e2=.2) Clr 3mm',...
                'Sgl LoE (e2=.2) Clr 6mm',...
                'Sgl Elec Abs Bleached 6mm',...
                'Sgl Elec Abs Colored 6mm',...
                'Sgl Elec Ref Bleached 6mm',...
                'Sgl Elec Ref Colored 6mm',...
                'Dbl Clr 3mm/6mm Air',...
                'Dbl Clr 3mm/13mm Air',...
                'Dbl Clr 3mm/13mm Arg',...
                'Dbl Clr 6mm/6mm Air',...
                'Dbl Clr 6mm/13mm Air',...
                'Dbl Clr 6mm/13mm Arg',...
                'Dbl Clr Low Iron 3mm/6mm Air',...
                'Dbl Clr Low Iron 3mm/13mm Air',...
                'Dbl Clr Low Iron 3mm/13mm Arg',...
                'Dbl Clr Low Iron 5mm/6mm Air',...
                'Dbl Clr Low Iron 5mm/13mm Air',...
                'Dbl Clr Low Iron 5mm/13mm Arg',...
                'Dbl Bronze 3mm/6mm Air',...
                'Dbl Bronze 3mm/13mm Air',...
                'Dbl Bronze 3mm/13mm Arg',...
                'Dbl Bronze 6mm/6mm Air',...
                'Dbl Bronze 6mm/13mm Air',...
                'Dbl Bronze 6mm/13mm Arg',...
                'Dbl Green 3m/6mm Air',...
                'Dbl Green 3mm/13mm Air',...
                'Dbl Green 3mm/13mm Arg',...
                'Dbl Green 6mm/6mm Air',...
                'Dbl Green 6mm/13mm Air',...
                'Dbl Green 6mm/13mm Arg',...
                'Dbl Grey 3mm/6mm Air',...
                'Dbl Grey 3mm/13mm Air',...
                'Dbl Grey 3mm/13mm Arg',...
                'Dbl Grey 6mm/6mm Air',...
                'Dbl Grey 6mm/13mm Air',...
                'Dbl Grey 6mm/13mm Arg',...
                'Dbl Blue 6mm/6mm Air',...
                'Dbl Blue 6mm/13mm Air',...
                'Dbl Blue 6mm/13mm Arg',...
                'Dbl Ref-A-L Clr 6mm/6mm Air',...
                'Dbl Ref-A-L Clr 6mm/13mm Air',...
                'Dbl Ref-A-L Clr 6mm/13mm Arg',...
                'Dbl Ref-A-M Clr 6mm/6mm Air',...
                'Dbl Ref-A-M Clr 6mm/13mm Air',...
                'Dbl Ref-A-M Clr 6mm/13mm Arg',...
                'Dbl Ref-A-H Clr 6mm/6mm Air',...
                'Dbl Ref-A-H 6mm/13mm Air',...
                'Dbl Ref-A-H Clr 6mm/13mm Arg',...
                'Dbl Ref-A-L Tint 6mm/6mm Air',...
                'Dbl Ref-A-L Tint 6mm/13mm Air',...
                'Dbl Ref-A-L Tint 6mm/13mm Arg',...
                'Dbl Ref-A-M Tint 6mm/6mm Air',...
                'Dbl Ref-A-M Tint 6mm/13mm Air',...
                'Dbl Ref-A-M Tint 6mm/13mm Arg',...
                'Dbl Ref-A-H Tint 6mm/6mm Air',...
                'Dbl Ref-A-H Tint 6mm/13mm Air',...
                'Dbl Ref-A-H Tint 6mm/13mm Arg',...
                'Dbl Ref-B-L Clr 6mm/6mm Air',...
                'Dbl Ref-B-L Clr 6mm/13mm Air',...
                'Dbl Ref-B-L Clr 6mm/13mm Arg',...
                'Dbl Ref-B-H Clr 6mm/6mm Air',...
                'Dbl Ref-B-H Clr 6mm/13mm Air',...
                'Dbl Ref-B-H Clr 6mm/13mm Arg',...
                'Dbl Ref-B-L Tint 6mm/6mm Air',...
                'Dbl Ref-B-L Tint 6mm/13mm Air',...
                'Dbl Ref-B-L Tint 6mm/13mm Arg',...
                'Dbl Ref-B-M Tint 6mm/6mm Air',...
                'Dbl Ref-B-M Tint 6mm/13mm Air',...
                'Dbl Ref-B-M Tint 6mm/13mm Arg',...
                'Dbl Ref-B-H Tint 6mm/6mm Air',...
                'Dbl Ref-B-H Tint 6mm/13mm Air',...
                'Dbl Ref B-H Tint 6mm/13mm Arg',...
                'Dbl Ref-C-L Clr 6mm/6mm Air',...
                'Dbl Ref-C-L Clr 6mm/13mm Air',...
                'Dbl Ref-C-L Clr 6mm/13mm Arg',...
                'Dbl Ref-C-M Clr 6mm/6mm Air',...
                'Dbl Ref-C-M Clr 6mm/13mm Air',...
                'Dbl Ref-C-M Clr 6mm/13mm Arg',...
                'Dbl Ref-C-H Clr 6mm/6mm Air',...
                'Dbl Ref-C-H Clr 6mm/13mm Air',...
                'Dbl Ref-C-H Clr 6mm/13mm Arg',...
                'Dbl Ref-C-L Tint 6mm/6mm Air',...
                'Dbl Ref-C-L Tint 6mm/13mm Air',...
                'Dbl Ref-C-L Tint 6mm/13mm Arg',...
                'Dbl Ref-C-M Tint 6mm/6mm Air',...
                'Dbl Ref-C-M Tint 6mm/13mm Air',...
                'Dbl Ref-C-M Tint 6mm/13mm Arg',...
                'Dbl Ref-C-H Tint 6mm/6mm Air',...
                'Dbl Ref-C-H Tint 6mm/13mm Air',...
                'Dbl Ref-C-H Tint 6mm/13mm Arg',...
                'Dbl Ref-D Clr 6mm/6mm Air',...
                'Dbl Ref-D Clr 6mm/13mm Air',...
                'Dbl Ref-D Clr 6mm/13mm Arg',...
                'Dbl Ref-D Tint 6mm/6mm Air',...
                'Dbl Ref-D Tint 6mm/13mm Air',...
                'Dbl Ref-D Tint 6mm/13mm Arg',...
                'Dbl LoE (e2=.4) Clr 3mm/6mm Air',...
                'Dbl LoE (e2=.4) Clr 3mm/13mm Air',...
                'Dbl LoE (e2=.4) Clr 3mm/13mm Arg',...
                'Dbl LoE (e2=.2) Clr 3mm/6mm Air',...
                'Dbl LoE (e2=.2) Clr 3mm/13mm Air',...
                'Dbl LoE (e2=.2) Clr 3mm/13mm Arg',...
                'Dbl LoE (e2=.2) Clr 6mm/6mm Air',...
                'Dbl LoE (e2=.2) Clr 6mm/13mm Air',...
                'Dbl LoE (e2=.2) Clr 6mm/13mm Arg',...
                'Dbl LoE (e2=.1) Clr 3mm/6mm Air',...
                'Dbl LoE (e2=.1) Clr 3mm/13mm Air',...
                'Dbl LoE (e2=.1) Clr 3mm/13mm Arg',...
                'Dbl LoE (e2=.1) Clr 6mm/6mm Air',...
                'Dbl LoE (e2=.1) Clr 6mm/13mm Air',...
                'Dbl LoE (e2=.1) Clr 6mm/13mm Arg',...
                'Dbl LoE (e2=.1) Tint 6mm/6mm Air',...
                'Dbl LoE (e2=.1) Tint 6mm/13mm Air',...
                'Dbl LoE (e2=.1) Tint 6mm/13mm Arg',...
                'Dbl LoE (e3=.1) Clr 3mm/6mm Air',...
                'Dbl LoE (e3=.1) Clr 3mm/13mm Air',...
                'Dbl LoE (e3=.1) Clr 3mm/13mm Arg',...
                'Dbl LoE Spec Sel Clr 3mm/6mm/6mm Air',...
                'Dbl LoE Spec Sel Clr 3mm/13mm/6mm Air',...
                'Dbl LoE Spec Sel Clr 3mm/13mm/6mm Arg',...
                'Dbl LoE Spec Sel Clr 6mm/6mm Air',...
                'Dbl LoE Spec Sel Clr 6mm/13mm Air',...
                'Dbl LoE Spec Sel Clr 6mm/13mm Arg',...
                'Dbl LoE Spec Sel Tint 6mm/6mm Air',...
                'Dbl LoE Spec Sel Tint 6mm/13mm Air',...
                'Dbl LoE Spec Sel Tint 6mm/13mm Arg',...
                'Dbl Elec Abs Bleached 6mm/6mm Air',...
                'Dbl Elec Abs Colored 6mm/6mm Air',...
                'Dbl Elec Abs Bleached 6mm/13mm Air',...
                'Dbl Elec Abs Colored 6mm/13mm Air',...
                'Dbl Elec Abs Bleached 6mm/13mm Arg',...
                'Dbl Elec Abs Colored 6mm/13mm Arg',...
                'Dbl Elec Ref Bleached 6mm/6mm Air',...
                'Dbl Elec Ref Colored 6mm/6mm Air',...
                'Dbl Elec Ref Bleached 6mm/13mm Air',...
                'Dbl Elec Ref Colored 6mm/13mm Air',...
                'Dbl Elec Ref Bleached 6mm/13mm Arg',...
                'Dbl Elec Ref Colored 6mm/13mm Arg',...
                'Dbl LoE Elec Abs Bleached 6mm/6mm Air',...
                'Dbl LoE Elec Abs Colored 6mm/6mm Air',...
                'Dbl LoE Elec Abs Bleached 6mm/13mm Air',...
                'Dbl LoE Elec Abs Colored 6mm/13mm Air',...
                'Dbl LoE Elec Abs Bleached 6mm/13mm Arg',...
                'Dbl LoE Elec Abs Colored 6mm/13mm Arg',...
                'Dbl LoE Elec Ref Bleached 6mm/6mm Air',...
                'Dbl LoE Elec Ref Colored 6mm/6mm Air',...
                'Dbl LoE Elec Ref Bleached 6mm/13mm Air',...
                'Dbl LoE Elec Ref Colored 6mm/13mm Air',...
                'Dbl LoE Elec Ref Bleached 6mm/13mm Arg',...
                'Dbl LoE Elec Ref Colored 6mm/13m Arg',...
                'Trp Clr 3mm/6mm Air',...
                'Trp Clr 3mm/13mm Air',...
                'Trp Clr 3mm/13mm Arg',...
                'Trp LoE (e5=.1) Clr 3mm/6mm Air',...
                'Trp LoE (e5=.1) Clr 3mm/13mm Air',...
                'Trp LoE (e5=.1) Clr 3mm/13mm Arg',...
                'Trp LoE (e2=e5=.1) Clr 3mm/6mm Air',...
                'Trp LoE (e2=e5=.1) Clr 3mm/13mm Air',...
                'Trp LoE (e2=e5=.1) Clr 3mm/13mm Arg',...
                'Trp LoE Film (88) Clr 3mm/6mm Air',...
                'Trp LoE Film (88) Clr 3mm/13mm Air',...
                'Trp LoE Film (77) Clr 3mm/6mm Air',...
                'Trp LoE Film (77) Clr 3mm/13mm Air',...
                'Trp LoE Film (66) Clr 6mm/6mm Air',...
                'Trp LoE Film (66) Clr 6mm/13mm Air',...
                'Trp LoE Film (66) Bronze 6mm/6mm Air',...
                'Trp LoE Film (66) Bronze 6mm/13mm Air',...
                'Trp LoE Film (55) Clr 6mm/6mm Air',...
                'Trp LoE Film (55) Clr 6mm/13m Air',...
                'Trp LoE Film (55) Bronze 6mm/6mm Air',...
                'Trp LoE Film (55) Bronze 6mm/13mm Air',...
                'Trp LoE Film (44) Bronze 6mm/6mm Air',...
                'Trp LoE Film (44) Bronze 6mm/13mm Air',...
                'Trp LoE Film (33) Bronze 6mm/6mm Air',...
                'Trp LoE Film (33) Bronze 6mm/13mm Air',...
                'Quadruple LoE Films (88) 3mm/8mm Krypton'
                };
    OPTIONS.DistributionExtWalls = 'Uniform';
    OPTIONS.RangeExtWalls(1) = 1e-3;
    OPTIONS.RangeExtWalls(2) = 2e-1;
    OPTIONS.DistributionRoof = 'Uniform';
    OPTIONS.RangeRoof(1) = 1e-3;
    OPTIONS.RangeRoof(2) = 2e-1;
    OPTIONS.version = '8.6.0';
    OPTIONS.echoFlag = 0;
end
%Library = OPTIONS.Library;
%DistributionExtWalls = OPTIONS.DistributionExtWalls;
%if strcmp(DistributionExtWalls, 'Uniform')
%    RangeExtWalls(1) = OPTIONS.RangeExtWalls(1);
%    RangeExtWalls(2) = OPTIONS.RangeExtWalls(2);
%else
%    MeanExtWalls = OPTIONS.MeanExtWalls;
%    StdExtWalls = OPTIONS.StdExtWalls;
%end
%DistributionRoof = OPTIONS.DistributionRoof;
%if strcmp(DistributionRoof, 'Uniform')
%    RangeRoof(1) = OPTIONS.RangeRoof(1);
%    RangeRoof(2) = OPTIONS.RangeRoof(2);
%else
%    MeanRoof = OPTIONS.MeanRoof;
%    StdRoof = OPTIONS.StdRoof;
%end

%% Building & Climate Zone
str = get(handles.popupmenu1,'String');
val = get(handles.popupmenu1,'Value');

Building = str{val};

str = get(handles.popupmenu2,'String');
val = get(handles.popupmenu2,'Value');

ClimateZone = str{val};

val = get(handles.DoPlot,'Value');
DoPlot = logical(val); %str2num(val);

%val = get(handles.Parallelize,'Value');
%Parallelize = logical(val); %str2num(val);


%% Input of Interests
inputOfInterest = {};

val = get(handles.ExtWallsUfactor,'Value');
InputFlags.ExtWallsUfactor = val; %str2num(val);

if InputFlags.ExtWallsUfactor
    inputOfInterest{end+1} = 'External Walls U-factor';
end

val = get(handles.RoofUfactor,'Value');
InputFlags.RoofUfactor = val; %str2num(val);

if InputFlags.RoofUfactor
    inputOfInterest{end+1} = 'Roof U-factor';
end

val = get(handles.GlazingUfactor,'Value');
InputFlags.GlazingUfactor = val; %str2num(val);

if InputFlags.GlazingUfactor
    inputOfInterest{end+1} = 'Glazing U-factor';
end

val = get(handles.GlazingSHGC,'Value');
InputFlags.GlazingSHGC = val; %str2num(val);

if InputFlags.GlazingSHGC
    inputOfInterest{end+1} = 'Glazing SHGC';
end

val = get(handles.WTF,'Value');
InputFlags.WTF = val; %str2num(val);

if InputFlags.WTF
    inputOfInterest{end+1} = 'Window-to-floor ratio';
end

assignin('base','inputOfInterest',inputOfInterest);
clear InputFlags


%% Output of Interests
outputOfInterest = {};
Outputs_ElectricityFacility = {'Daily Electricity Facility', 'Monthly Electricity Facility', 'Electricity Facility'};
Outputs_GasFacility = {'Daily Gas Facility', 'Monthly Gas Facility', 'Gas Facility'};
Outputs_ElectricityBuilding = {'Daily Electricity Building', 'Monthly Electricity Building', 'Electricity Building'};
Outputs_GasBuilding = {'Daily Gas Building', 'Monthly Gas Building', 'Gas Building'};
Outputs_ElectricityHVAC = {'Daily Electricity HVAC', 'Monthly Electricity HVAC', 'Electricity HVAC'};
Outputs_GasHVAC = {'Daily Gas HVAC', 'Monthly Gas HVAC', 'Gas HVAC'};
Outputs_ElectricityPlant = {'Daily Electricity Plant', 'Monthly Electricity Plant', 'Electricity Plant'};
Outputs_GasPlant = {'Daily Gas Plant', 'Monthly Gas Plant', 'Gas Plant'};

DataGranularity = zeros(1,3);

val = get(handles.Daily,'Value');
DataGranularity(1) = val;

val = get(handles.Monthly,'Value');
DataGranularity(2) = val;

val = get(handles.RunPeriod,'Value');
DataGranularity(3) = val;

DataGranularity = logical(DataGranularity);

val = get(handles.ElectricityFacility,'Value');
OutputFlags.ElectricityFacility = val; 



if OutputFlags.ElectricityFacility
    selection = Outputs_ElectricityFacility(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.GasFacility,'Value');
OutputFlags.GasFacility = val; %str2num(val);

if OutputFlags.GasFacility
    selection = Outputs_GasFacility(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.ElectricityBuilding,'Value');
OutputFlags.ElectricityBuilding = val; %str2num(val);

if OutputFlags.ElectricityBuilding
    selection = Outputs_ElectricityBuilding(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.GasBuilding,'Value');
OutputFlags.GasBuilding = val; %str2num(val);

if OutputFlags.GasBuilding
    selection = Outputs_GasBuilding(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.ElectricityHVAC,'Value');
OutputFlags.ElectricityHVAC = val; %str2num(val);

if OutputFlags.ElectricityHVAC
    selection = Outputs_ElectricityHVAC(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.GasHVAC,'Value');
OutputFlags.GasHVAC = val; %str2num(val);

if OutputFlags.GasHVAC
    selection = Outputs_GasHVAC(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.ElectricityPlant,'Value');
OutputFlags.ElectricityPlant = val; %str2num(val);

if OutputFlags.ElectricityPlant
    selection = Outputs_ElectricityPlant(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

val = get(handles.GasPlant,'Value');
OutputFlags.GasPlant = val; %str2num(val);

if OutputFlags.GasPlant
    selection = Outputs_GasPlant(DataGranularity);
    for j = 1:length(selection)
        outputOfInterest{end+1} = selection{j};
    end
end

assignin('base','outputOfInterest',outputOfInterest);
clear OutputFlags

InputFolder = get(handles.InputFolder,'String');
OutputFolder = get(handles.OutputFolder,'String');
EnergyPlusPath = get(handles.EnergyPlusPath,'String');
nrSim = get(handles.nrSim,'String');
nrSim = str2double(nrSim);

%% Launch simulation
APSEplus_LAUNCHER
assignin('base','SimOutput',SimOutput);
assignin('base','SimInput',SimInput);
assignin('base','SIMUL',SIMUL);
assignin('base','time',time);


% --- Executes during object creation, after setting all properties.
function SimulationsGUI_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SimulationsGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on SimulationsGUI and none of its controls.
function SimulationsGUI_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to SimulationsGUI (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object deletion, before destroying properties.
function SimulationsGUI_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to SimulationsGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function EnergyPlusPath_Callback(hObject, eventdata, handles)
% hObject    handle to EnergyPlusPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of EnergyPlusPath as text
%        str2double(get(hObject,'String')) returns contents of EnergyPlusPath as a double


% --- Executes during object creation, after setting all properties.
function EnergyPlusPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EnergyPlusPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.op
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --------------------------------------------------------------------
function Options_Callback(hObject, eventdata, handles)
subGUI = ASPEplusOptions;
SimulationsGUI = findobj('Tag','SimulationsGUI');
waitfor(subGUI)
subGUI_handles = guidata(SimulationsGUI);
handles.OPTIONS = subGUI_handles.OPTIONS;
assignin('base','OPTIONS', handles.OPTIONS);


% --- Executes on button press in checkbox21.
function checkbox21_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox21


% --- Executes on button press in checkbox22.
function checkbox22_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox22
