%% CHECK IDF FILE
% This code checks is the selected idf file is updated to the version
% of EnergyPlus chosen.

fid = fopen(strcat(InputFolder, SIMUL.FolderName, SIMUL.building, '.idf'),'r+');
found_Version = 0;

while found_Version == 0
    tline = fgetl(fid);
    found_Version = (isempty(strfind(tline, '  Version'))==0);

    if any(tline == -1)
        break
    end
end

[~,vv] = strtok(tline,',');
vv = vv(2:end-1);

if isempty(strfind(SIMUL.version, vv))
    error('Wrong IDF file version:\n IDF file has version %s\n You selected EnergyPlus %s. \n Please update it with IDF Version Updater.',vv, SIMUL.version);
end
