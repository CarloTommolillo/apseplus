%% CHECK WEATHER FILE
% Checks if the selected weather file is located in the weather file
% folder.

fid = fopen(strcat(EnergyPlusPath, 'WeatherData\', SIMUL.weatherfile, '.epw'),'r+');
if fid == -1
    error('The weather file\n %s\n is not present in the folder\n %s.\n Please, provide it or choose a different weather file.', ...
        SIMUL.weatherfile, strcat(EnergyPlusPath, 'WeatherData\'))
end
