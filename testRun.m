% Do some tasks

% Call sleep.exe for 30 seconds 
% (sleep makes dos "idle" for the given time, simulating if the exe were running)


% MATLAB will continue execution once EXE is launched
disp('MATLAB continues after calling EXE')

% Check to see if the EXE process exists
flag = true;
while flag
    disp('EXE still running');   
    flag = isprocess('C:\EnergyPlusV8-4-0\RunEplus C:\Users\Carlo\Desktop\EnergyPlusOUTPUT\RefBldgSmallOfficeNew2004_Chicago_\RefBldgSmallOfficeNew2004_Chicago'); % isprocess is the function attached. Place it on the MATLAB path.
    pause(1) % check every 1 second
end

disp('EXE Done')
disp('continuing with MATLAB script')