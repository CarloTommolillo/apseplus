close all
clear all
clc

addpath('C:\Users\Carlo\Desktop\EnergyPlusOUTPUT\RefBldgSmallOfficeNew2004_Chicago_');
addpath('C:\Users\Carlo\Documents\MATLAB\calafiore-novara');
addpath('C:\Users\Carlo\Documents\MATLAB\mlep\MLE+\core');

SIMUL.building = 'RefBldgSmallOfficeNew2004_Chicago';
SIMUL.nrZones = 6;
SIMUL.TotalFloorArea = 551;
SIMUL.TotalWindowArea = 55.8;
SIMUL.MaxMultiplier = 4.5; % TotalWallArea = 281,515
                         % TotalWindowArea/TotalWallArea = (55,8 +
                         % 3,9)/281,515 = 0,212 = 1/4,71

nrSim = 2;
KeepAllIDF = 0;
DoPlot = 0;
DoPlotTemp = 0;

outputMainFolder= 'C:\Users\Carlo\Desktop\EnergyPlusOUTPUT\';

% NB: parameters inside the same section must be toghether.
inputOfInterest = {'External Walls U-factor',
                   'Roof U-factor',
                   'Glazing U-factor',
                   'Glazing SHGC',
                   'Window-to-floor ratio',
                   %'Window Height'
                   };
               
outputOfInterest = {%'Monthly Electricity consumption'...
                    %'Monthly Gas consumption'...
                    'Electricity consumption'...
                    'Gas consumption'...
                    'Electricity Building'...
                    'Electricity HVAC'...
                    'Gas HVAC'...
                    'Electricity Plant'...
                    'Gas Plant'
                    %'Average Temperature Deviation'
                    };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SIMULATION SETUP 
SIMUL.setup = [];

nInputs = length(inputOfInterest);

% Settings for each material and parameter
for i = 1:nInputs
    switch inputOfInterest{i}
        case 'External Walls U-factor'
            % CORRESPONDING TO: Walls Insulation Thickness
            SIMUL.setup{end+1} = [];
            SIMUL.setup{end}.section = 'Material';
            SIMUL.setup{end}.surfacetype= 'Mass NonRes Wall Insulation';
            SIMUL.setup{end}.parameter = 'Thickness';
            SIMUL.setup{end}.nparameter = 1;
            SIMUL.setup{end}.type = 'Material Parameter';
            SIMUL.setup{end}.distribution = 'Uniform';
            SIMUL.setup{end}.range = [1.0E-03 , 2.0E-01]; %range?
        case 'Roof U-factor'
            % CORRESPONDING TO: Walls Insulation Thickness
            SIMUL.setup{end+1} = [];
            SIMUL.setup{end}.section = 'Material';
            SIMUL.setup{end}.surfacetype= 'AtticFloor NonRes Insulation';
            %SIMUL.setup{end}.surfacetype= 'Roof Membrane';
            SIMUL.setup{end}.parameter = 'Thickness';
            SIMUL.setup{end}.nparameter = 1;
            SIMUL.setup{end}.type = 'Material Parameter';
            SIMUL.setup{end}.distribution = 'Uniform';
            %SIMUL.setup{end}.range = [1.0E-03 , 2.0E-01]; %range?
            SIMUL.setup{end}.range = [1.0E-03 , 2.0E-02]; %range?
        case 'Glazing U-factor'
            % CORRESPONDING TO: Type of Glazing 
            SIMUL.setup{end+1} = [];
            SIMUL.setup{end}.section = 'FenestrationSurface:Detailed';
            SIMUL.setup{end}.surfacetype= 'Window,                  !- Surface Type';
            SIMUL.setup{end}.parameter = 'Construction Name';
            SIMUL.setup{end}.nparameter = 1;
            SIMUL.setup{end}.type = 'Surface Configuration';
            SIMUL.setup{end}.distribution = 'Discrete uniform selection';
        case 'Glazing SHGC'
            % CORRESPONDING TO: Type of Glazing 
            SIMUL.setup{end+1} = [];
            SIMUL.setup{end}.section = 'FenestrationSurface:Detailed';
            SIMUL.setup{end}.surfacetype= 'Window,                  !- Surface Type';
            SIMUL.setup{end}.parameter = 'Construction Name';
            SIMUL.setup{end}.nparameter = 1;
            SIMUL.setup{end}.type = 'Surface Configuration';
            SIMUL.setup{end}.distribution = 'Discrete uniform selection';
        case 'Window-to-floor ratio'
            % CORRESPONDING TO: Window Size 
            SIMUL.setup{end+1} = [];
            SIMUL.setup{end}.section = 'FenestrationSurface:Detailed';
            SIMUL.setup{end}.surfacetype= 'Window,                  !- Surface Type';
            SIMUL.setup{end}.parameter = 'Multiplier';
            SIMUL.setup{end}.nparameter = 1;
            SIMUL.setup{end}.type = 'Surface Configuration Parameter';
            SIMUL.setup{end}.range = [1 , SIMUL.MaxMultiplier]; %range?
            SIMUL.setup{end}.distribution = 'Uniform';
        case 'Window Height'
            % CORRESPONDING TO: Window Size 
            SIMUL.setup{end+1} = [];
            SIMUL.setup{end}.section = 'FenestrationSurface:Detailed';
            SIMUL.setup{end}.surfacetype= 'Window,                  !- Surface Type';
            SIMUL.setup{end}.parameter = {'Vertex 1 Z-coordinate',...
                                          'Vertex 2 Z-coordinate',...
                                          'Vertex 3 Z-coordinate',...
                                          'Vertex 4 Z-coordinate';
                                          2 , 1 , 1 , 2}; % first row is the name, second row is the value to be used
            SIMUL.setup{end}.nparameter = length(SIMUL.setup{end}.parameter);                          
            SIMUL.setup{end}.type = 'Surface Configuration Parameter';
            SIMUL.setup{end}.range = [0.2 , 1.1; 2.4 , 2.8]; 
            SIMUL.setup{end}.distribution = 'Uniform';
    end
end

testName = strcat(SIMUL.building,'_');
%for i=1:length(SIMUL.setup)
%    material = SIMUL.setup{i}.material;
%	parameter = SIMUL.setup{i}.parameter;
%	range = SIMUL.setup{i}.range;
%	distribution = SIMUL.setup{i}.distribution;
    
%	testName = strcat(testName, regexprep(material, ' ','_'), regexprep(parameter, ' ','_'));
%end

% Create output folder
mkdir(outputMainFolder,testName);
outputFolder = strcat(outputMainFolder,testName, '\');

% Keep all IDF files?
if KeepAllIDF == 0
    copyfile(strcat(outputMainFolder, SIMUL.building, '.idf'), outputFolder)
end

initializationTime = zeros(1,nrSim);
countIter = zeros(1,nrSim);

err_count = 0;
for i = 1:nrSim
    try
        clc
        disp('-----------------------------------------------------------------');
        disp('-----------------------------------------------------------------');
        disp(['START SIMULATION ' num2str(i)]);
        disp(['ABORTED SIMULATIONS ' num2str(err_count)]);
        if KeepAllIDF
            copyfile(strcat(outputMainFolder, SIMUL.building, '.idf'), outputFolder)
        end
        SimOutput{i} = [];
        SimInput{i} = [];

        %% SIMULATE
        [SimOutput{i}, SimInput{i}, initializationTime(i), countIter(i)] = SimulateWithEnergyPlus(SIMUL, outputOfInterest, outputFolder, inputOfInterest);
        if KeepAllIDF
            % Rename IDF file
            cd(outputFolder)
            movefile(strcat(SIMUL.building,'.idf'),strcat(SIMUL.building,'_',num2str(i),'.idf'));
        end
        disp('-----------------------------------------------------------------');
        disp(['END SIMULATION ' num2str(i)]);
    catch
        err_count = err_count + 1;
    end      
end

save('RESULTS1000.mat')

if DoPlot
     PlotSimulationResults(SimOutput, SimInput, outputOfInterest, inputOfInterest, SIMUL);
end

if DoPlotTemp
    figure
    hold on
    plot(SimOutput{1,1}.temp(:,1),'k')
    plot(SimOutput{1,1}.CoolingSetpoint(:,1),'b')
    plot(SimOutput{1,1}.HeatingSetpoint(:,1),'r')
    hold off
end



