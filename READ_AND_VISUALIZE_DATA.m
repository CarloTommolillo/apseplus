close all

%% Choose input variables
%InVar = {'External Walls U-factor'};
%InVar = {'Glazing U-factor'};
%InVar = {'Glazing SHGC'};%
%InVar = {'Window-to-floor ratio'};
%InVar = {'Roof U-factor'};
InVar = {'All'};

%% Choose output variables
%OutVar = {'Electricity Facility'};
%OutVar = {'Gas Facility'};
%OutVar = {'Electricity Building'};
%OutVar = {'Gas Building'};
%OutVar = {'Electricity HVAC'};
%OutVar = {'Gas HVAC'};
%OutVar = {'Electricity Plant'};
%OutVar = {'Gas Plant'};
OutVar = {'All'};


n = length(SimOutput);

nIn = 1;

labelx = [];
labely = [];

%% Read input-variables
if length(InVar) == 1 
    if strcmp(InVar{1},'All') == 1
        InVars = {'External Walls U-factor'; 'Roof U-factor'; 'Glazing U-factor'; 'Glazing SHGC'; 'Window-to-floor ratio'};
        nIn = length(InVars);
    else
        InVars = InVar;
        nIn = 1;
    end
else
    % Multiple input-variables selected, but not all of them
    InVars = InVar;
    nIn = length(InVars);
end

X = [];
for i = 1:nIn
    switch InVars{i}
        case 'External Walls U-factor'
            xj = [];
            for j=1:n
                xj = [xj; SimInput{j}.ExtWallsUfactor];
            end
            X = [X xj];
            labelx{i} = {'External Walls U-factor'; '[W/(m^2K)]'};
            
         case 'Roof U-factor'
             xj = [];
            for j=1:n
                xj = [xj; SimInput{j}.RoofUfactor];
            end
            X = [X xj];
            labelx{i} = {'Roof U-factor'; '[W/(m^2K)]'};

        case 'Glazing U-factor'
            xj = [];
            for j=1:n
                xj = [xj; SimInput{j}.GlazingUfactor];
            end
            X = [X xj];
            labelx{i} = {'Glazing U-factor'; '[W/(m^2K)]'};

        case 'Glazing SHGC'
            xj = [];
            for j=1:n
                xj = [xj; SimInput{j}.GlazingSHGC];
            end
            X = [X xj];
            labelx{i} = 'Glazing SHGC';

        case 'Window-to-floor ratio'
            xj = [];
            for j=1:n
                xj = [xj; SimInput{j}.WFR];
            end
            X = [X xj];
            labelx{i} = {'Window-to-floor ratio'};
    end        
end

%% Read output-variables
if length(OutVar) == 1
    if strcmp(OutVar{1},'All') == 1
        OutVars = {'Electricity Facility'; 'Gas Facility'; 'Electricity Building'; 'Gas Building'; 'Electricity HVAC'; 'Gas HVAC'; 'Electricity Plant'; 'Gas Plant';};
        nOut = length(OutVars);
    else
        OutVars = OutVar;
        nOut = 1;
    end
else
    % Multiple input-variables selected, but not all of them
    OutVars = OutVar;
    nOut = length(OutVars);
end


Y = [];
for i = 1:nOut
    switch OutVars{i}
        case 'Electricity Facility'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.Electricityconsumption];
            end
            Y = [Y yj];
            labely{i} = {'Electricity Facility'; '[J]'};
            
         case 'Gas Facility'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.Gasconsumption];
            end
            Y = [Y yj];
            labely{i} = {'Gas Facility'; '[J]'};

        case 'Electricity Building'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.ElectricityBuilding];
            end
            Y = [Y yj];
            labely{i} = {'Electricity Building'; '[J]'};

        case 'Gas Building'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.Gasconsumption-(SimOutput{j}.GasHVAC + SimOutput{j}.GasPlant)];
            end
            Y = [Y yj];
            labely{i} = {'Gas Building'; '[J]'}; 

        case 'Electricity HVAC'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.ElectricityHVAC];
            end
            Y = [Y yj];
            labely{i} = {'Electricity HVAC'; '[J]'};
            
        case 'Gas HVAC'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.GasHVAC];
            end
            Y = [Y yj];
            labely{i} = {'Gas HVAC'; '[J]'};
            
        case 'Electricity Plant'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.ElectricityPlant];
            end
            Y = [Y yj];
            labely{i} = {'Electricity Plant'; '[J]'};
            
        case 'Gas Plant'
            yj = [];
            for j=1:n
                yj = [yj; SimOutput{j}.GasPlant];
            end
            Y = [Y yj];
            labely{i} = {'Gas Plant'; '[J]'};
    end
end
    
    %% Plot Results
    for k = 1:nIn
        FigHandle = figure(k);
        set(FigHandle, 'Position', [100, 100, 1400, 895]);
        hold on
        for j = 1:nOut
            subplot(2,ceil(nOut/2),j);
            temp = strfind(labely{j}(1),'Gas');
            if (isempty(temp{1})==0)
                plot(X(:,k),Y(:,j),'.r','MarkerSize',5)
            else
                plot(X(:,k),Y(:,j),'.','MarkerSize',5)
                %set(gca,'YTickLabel',num2str(get(gca,'YTick').'))
                ax = gca;
                ax.YAxis.TickLabelFormat = '%,.1f';
            end
            ylabel(labely{j},'FontSize',12)
            xlabel(labelx{k},'FontSize',12)
            hold on
        end
        hold off
    end